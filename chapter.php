<?php
include('admin/config.php');
$bid = $_GET['bid'];
if (isset($bid)) {
    $query_book = "SELECT * FROM book WHERE id='" . $bid . "'";
}
$result_book = mysqli_query($con, $query_book);
$row_book = $result_book->fetch_assoc();
//$total_chap = $row_book['page_no'];
$row_cnt = $result_book->num_rows;

$query_page = "SELECT image_name FROM user_uploads WHERE user_id='" . $bid . "' ORDER BY id ASC";
$rs_page = mysqli_query($con, $query_page);
while ($row_page = $rs_page->fetch_assoc()) {
    $page_no[] = $row_page['image_name'];
    //echo "<pre>".print_r($row_page);
}
$page_cnt = $rs_page->num_rows;
$total_chap = $page_cnt;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e - library | Hi-Lab Solution </title>
        <link type="text/css" rel="stylesheet" href="assest/css/main_search.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/style.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/media_query.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/jquery_ui.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/bgstretcher/bgstretcher.css" />
    </head>

    <body>
        <div id="loader" style="display:none; position:absolute; top:49%; left:49%;">
            <img src="assest/images/ajax-loader.gif" />
        </div>

        <img id="wallbg" src="assest/images/my-20corner-20slices/wall.jpg" class="bgstretcher" style="z-index:-1;" />

        <div id="body_container">
            <div id="logo" class="muse_logo">
                <img src="assest/images/my-20corner-20slices/logo.png" alt="Muse Logo" />
            </div>
            <div id="shelf_book" class="shelf_book_container">
                <?php include 'searchbars.php'; ?>
                <?php
                $cnt = ceil($total_chap / 9);
                for ($i = 0; $i < $cnt; $i++) {
                    ?>
                    <div class="shelf3_container">
                        <div class="shelf3_img">
                            <img src="assest/images/my-20corner-20slices/shelf.png" />
                        </div>
                    </div>
                <?php } ?>

                <div id="book_shelf" class="shelf_books_container" style="position:absolute;">
                    <div id="shelf1">
                        <div id="shelf_block1" class="shelf3_block1_container">
                            <?php
                            $i = 0;
                            foreach ($page_no as $chap_no) {
                                $pdf_name = explode(".", $chap_no);
                                $pdf_name = $pdf_name[0];
                                ?>
                                <div class="shelf3_block1_img">
                                    <div id="bbook<?php echo $i; ?>" class="bubble">
                                        <img src="assest/images/buncee_clipart_bubble_26.png" />
                                        <div id="bbt<?php echo $i; ?>" class="bubble_text"><?php echo $pdf_name; ?></div>
                                    </div>
                                    <a href="upload/<?php echo $row_book['name']; ?>/<?php echo $chap_no; ?>">
                                        <img id="book<?php echo $i; ?>" src="img/cover.png" />
                                    </a>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>  
            </div>
        <?php include('dock.php'); ?>
        </div>
<?php include('bottom.php'); ?>        
    </body>
</html>

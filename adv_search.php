<?php

include('admin/config.php');

/* * **********query for standard selection******** */
$query_std = "SELECT * FROM standard where search_display='y'";
$result_std = mysqli_query($con, $query_std);
while ($row_std = $result_std->fetch_assoc()) {
    $arr_std[] = "standard REGEXP '0" . $row_std['standard'] . "'";
    //$arr_std1[] = "0" . $row_std['standard'];
}

/* * **********query for type selection******** */
$query_type = "SELECT * FROM type where search_display='y'";
$result_type = mysqli_query($con, $query_type);
while ($row_type = $result_type->fetch_assoc()) {
    $arr_type[] = "type REGEXP '0" . $row_type['id'] . "'";
    //$arr_type1[] = "0" . $row_type['id'];
}

/* * **********query for age selection******** */
$query_age = "SELECT * FROM age where search_display='y'";
$result_age = mysqli_query($con, $query_age);
while ($row_age = $result_age->fetch_assoc()) {
    $arr_age[] = "age REGEXP '0" . $row_age['age'] . "'";
    //$arr_age1[] = "0" . $row_age['age'];
}

/* * **********query for subject selection******** */
$query_sub = "SELECT * FROM subject where search_display='y'";
$result_sub = mysqli_query($con, $query_sub);
while ($row_sub = $result_sub->fetch_assoc()) {
    $arr_sub[] = "subject REGEXP '0" . $row_sub['id'] . "'";
    //$arr_sub1[] = "0" . $row_sub['id'];
}

$str_std = implode(' or ', $arr_std); //echo $str_std;  echo "<br>";
$str_type = implode(' or ', $arr_type); //echo $str_type; die();
$str_age = implode(' or ', $arr_age); //echo $str_age; die();
$str_sub = implode(' or ', $arr_sub); //echo $str_sub; die();

// Define Output HTML Formating
$html = '';
$html .= '<li class="result">';
$html .= '<a href="search.php?id=idString">';
$html .= '<h3>nameString</h3>';
$html .= '<h4><b>Type:</b> typeString</h4>';
$html .= '<h4><b>Subject:</b> subjString</h4>';
$html .= '</a>';
$html .= '</li>';

// Get Search
$search_string = preg_replace("/[^A-Za-z0-9]/", " ", $_POST['query']);
$search_string = $con->real_escape_string($search_string);

// Check Length More Than One Character
if (strlen($search_string) >= 1 && $search_string !== ' ') {
    // Build Query
    $query = "SELECT * FROM book WHERE name LIKE '%$search_string%' AND (" . $str_std . ") AND (" . $str_age . ") AND (" . $str_sub . ")";

    // Do Search
    $result = mysqli_query($con, $query);
    while ($results = $result->fetch_array()) {
        $result_array[] = $results;
    }

    // Check If We Have Results
    if (isset($result_array)) {
        foreach ($result_array as $result) {
            $type = "SELECT * FROM type WHERE id='".$result['type']."'";
            $type_rs = mysqli_query($con,$type);
            $type_row = $type_rs->fetch_assoc();
            
            $subject = "SELECT * FROM subject WHERE id='".$result['subject']."'";
            $subject_rs = mysqli_query($con,$subject);
            $subject_row = $subject_rs->fetch_assoc();
            
            // Format Output Strings And Hightlight Matches
            //$display_function = preg_replace("/".$search_string."/i", "<b class='highlight'>".$search_string."</b>", $result['function']);
            $display_name = preg_replace("/" . $search_string . "/i", "<b class='highlight'>" . $search_string . "</b>", $result['name']);
            $display_type = preg_replace("/" . $search_string . "/i", "<b class='highlight'>" . $search_string . "</b>", $type_row['type']);
            $display_subj = preg_replace("/" . $search_string . "/i", "<b class='highlight'>" . $search_string . "</b>", $subject_row['subject']);
            $display_id = $result['id'];

            // Insert Name
            $output = str_replace('nameString', $display_name, $html);
            $output = str_replace('typeString', $display_type, $output);
            $output = str_replace('subjString', $display_subj, $output);
            $output = str_replace('idString', $display_id, $output);

            // Output
            echo($output);
        }
    } else {

        // Format No Results Output
        $output = str_replace('nameString', '<b>No Results Found.</b>', $html);
        $output = str_replace('typeString', 'Sorry', $output);
        $output = str_replace('subjString', '', $output);

        // Output
        echo($output);
    }
}
?>
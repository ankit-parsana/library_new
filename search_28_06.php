<?php
include('admin/config.php');
/* * **********query for standard selection******** */
$query_std = "SELECT * FROM standard where search_display='y'";
$result_std = mysqli_query($con, $query_std);
while ($row_std = $result_std->fetch_assoc()) {
    $arr_std[] = "0".$row_std['standard'];
} //print_r($arr);

/* * **********query for type selection******** */
$query_type = "SELECT * FROM type where search_display='y'";
$result_type = mysqli_query($con, $query_type);
while ($row_type = $result_type->fetch_assoc()) {
    $arr_type[] = "0".$row_type['id'];
}

/* * **********query for age selection******** */
$query_age = "SELECT * FROM age where search_display='y'";
$result_age = mysqli_query($con, $query_age);
while ($row_age = $result_age->fetch_assoc()) {
    $arr_age[] = "0".$row_age['age'];
} //print_r($arr);

/* * **********query for subject selection******** */
$query_sub = "SELECT * FROM subject where search_display='y'";
$result_sub = mysqli_query($con, $query_sub);
while ($row_sub = $result_sub->fetch_assoc()) {
    $arr_sub[] = "0".$row_sub['id'];
} //print_r($arr);

$str_std = implode(',', $arr_std); //echo $str_std;  die();
$str_type = implode(',', $arr_type); //echo $str_type; die();
$str_age = implode(',', $arr_age); //echo $str_age; die();
$str_sub = implode(',', $arr_sub); //echo $str_sub; die();

$id = $_GET['id'];
$chk = $_GET['chk'];
$cat = $_GET['cat'];

if (isset($id)) {
    $query_book = "SELECT * FROM book WHERE id='$id' && standard IN(".$str_std.") && age IN(".$str_age.") && subject IN(" . $str_sub . ") && type IN(" . $str_type . ")";
}else if (isset($cat)) {
    $cat = '0'.$cat.',';
    $query_book = "SELECT * FROM book WHERE type LIKE '%$cat%' && standard IN(".$str_std.") && age IN(".$str_age.") && subject IN(" . $str_sub . ") && type IN(" . $str_type . ")";
} else if (isset($chk)) {
    $age = $_POST['age'];
    $type = $_POST['type'];
    $std = $_POST['std'];
    $sub = $_POST['sub'];

    $query_book = "SELECT * FROM book where standard IN(".$str_std.") && age IN(".$str_age.") && subject IN(" . $str_sub . ") && type IN(" . $str_type . ")";
    //$query_book = "SELECT * FROM book where type IN(" . $str_type . ")";

    if ($age != null && $age != "") {
        $query_book.=" && age='" . $age . "'";
    }
    if ($type != null && $type != "") {
        $query_book.=" && type='" . $type . "'";
    }
    if ($std != null && $std != "") {
        $query_book.=" && standard='" . $std . "'";
    }
    if ($sub != null && $sub != "") {
        $query_book.=" && subject='" . $sub . "'";
    }

    $query_book.="ORDER BY name ASC";

    //$query_book = "SELECT * FROM book WHERE age='".$age."' && type='$type' && standard='$std' && subject='$sub' && standard IN(".$str.")";
} else {
    $query_book = "SELECT * FROM book where standard IN(".$str_std.") && age IN(".$str_age.") && subject IN(" . $str_sub . ") && type IN(" . $str_type . ") ORDER BY name ASC";
    //$query_book1 = "SELECT count(*) FROM book where standard IN(" . $str . ") ORDER BY name ASC";
}
$result_book = mysqli_query($con, $query_book);
$row_cnt = $result_book->num_rows;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e - library | Hi-Lab Solution </title>
        <link type="text/css" rel="stylesheet" href="assest/css/main_search.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/style.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/media_query.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/jquery_ui.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/bgstretcher/bgstretcher.css" />
    </head>

    <body>
        <div id="loader" style="display:none; position:absolute; top:49%; left:49%;">
            <img src="assest/images/ajax-loader.gif" />
        </div>

        <img id="wallbg" src="assest/images/my-20corner-20slices/wall.jpg" class="bgstretcher" style="z-index:-1;" />

        <div id="body_container">
            <div id="logo" class="muse_logo">
                <img src="assest/images/my-20corner-20slices/logo.png" alt="Muse Logo" />
            </div>
            <div id="shelf_book" class="shelf_book_container">
                <?php include 'searchbars.php'; ?>
                <?php
                $cnt = ceil($row_cnt / 9);
                for ($i = 0; $i < $cnt; $i++) {
                    ?>
                    <div class="shelf3_container">
                        <div class="shelf3_img">
                            <img src="assest/images/my-20corner-20slices/shelf.png" />
                        </div>
                    </div>
                <?php } ?>

                <div id="book_shelf" class="shelf_books_container" style="position:absolute;">
                    <div id="shelf1">
                        <div id="shelf_block1" class="shelf3_block1_container">
                            <?php
                            $i = 1;
                            while ($row_book = $result_book->fetch_assoc()) { //echo "<pre>".print_r($row_book);
                                if ($row_book['book_type'] == 'photo') {
                                    ?>
                                    <div class="shelf3_block1_img">
                                        <div id="bbook<?php echo $i; ?>" class="bubble">
                                            <img src="assest/images/buncee_clipart_bubble_26.png" />
                                            <div id="bbt<?php echo $i; ?>" class="bubble_text"><?php echo $row_book['name']; ?></div>
                                        </div>
                                        <a href="slider.php?bid=<?php echo $row_book['id']; ?>">
                                            <img id="book<?php echo $i; ?>" src="upload/<?php echo $row_book['name']; ?>/1.jpg" />
                                        </a>
                                    </div>
                                <?php } elseif ($row_book['book_type'] == 'pdf') {
                                    ?>
                                    <div class="shelf3_block1_img">
                                        <div id="bbook<?php echo $i; ?>" class="bubble">
                                            <img src="assest/images/buncee_clipart_bubble_26.png" />
                                            <div id="bbt<?php echo $i; ?>" class="bubble_text"><?php echo $row_book['name']; ?></div>
                                        </div>
                                        <a href="chapter.php?bid=<?php echo $row_book['id']; ?>">
                                            <img id="book<?php echo $i; ?>" src="img/cover.png" />
                                        </a>
                                    </div>
                                    <?php
                                }
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>  
            </div>
             <?php include('dock.php'); ?>
        </div>
            <?php include('bottom.php'); ?>
    </body>
</html>

<?php
include('admin/config.php');
$query_cat = "SELECT * FROM type WHERE search_display='y'";
$result_cat = mysqli_query($con, $query_cat);
while ($row_cat = $result_cat->fetch_assoc()) {
    //echo "<pre>".  print_r($row_cat);
    $cat_arr[] = $row_cat['id'];
}
$row_cnt = $result_cat->num_rows;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e - library | Hi-Lab Solution </title>
        <link type="text/css" rel="stylesheet" href="assest/css/main.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/style.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/media_query_dash.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/jquery_ui.css" />
        <link type="text/css" rel="stylesheet" href="assest/css/bgstretcher/bgstretcher.css" />
    </head>

    <body>
        <div id="loader" style="display:none; position:absolute; top:49%; left:49%;">
            <img src="assest/images/ajax-loader.gif" />
        </div>

        <img id="wallbg" src="assest/images/my-20corner-20slices/wall.jpg" class="bgstretcher" style="z-index:-1;" />

        <div id="body_container">

            <!--<div class="header_line">
            <img src="assest/images/my-20corner-20slices/header line.jpg" alt="Header Line" />
        </div>-->

            <div id="logo" class="muse_logo">
                <img src="assest/images/my-20corner-20slices/logo.png" alt="Muse Logo" />
            </div>
            <div id="shelf_book" class="shelf_book_container">
                <?php if ($row_cnt <= 3) { ?>
                    <style>
                        .shelf_books_container {
                            top: 47% !important;
                        }
                    </style>
                    <?php
                }
                //include 'searchbars.php';
                $cnt = ceil($row_cnt / 3);
                for ($i = 0; $i < $cnt; $i++) {
                    ?>
                    <div class="shelf3_container">
                        <div class="shelf3_img">
                            <img src="assest/images/my-20corner-20slices/shelf.png" />
                        </div>
                    </div>
                <?php } ?>

                <div id="book_shelf" class="shelf_books_container">
                    <div id="shelf1">
                        <?php
                        $i = 1;
                        foreach ($cat_arr as $cat) {
                            $query_book = "SELECT * FROM book WHERE type='$cat' ORDER BY id DESC LIMIT 3";
                            $result_book = mysqli_query($con, $query_book);
                            ?>
                            <div id="shelf_block" class="shelf3_block1_container">
                                <?php
                                $j = 1;
                                while ($row_book = $result_book->fetch_assoc()) {
                                    ?>
                                    <div class="shelf3_block1_img">
                                        <div id="bbook<?php echo $row_book['id']; ?>" class="bubble">
                                            <img src="assest/images/buncee_clipart_bubble_26.png" />
                                            <div id="bbt<?php echo $row_book['id']; ?>" class="bubble_text"><?php echo $row_book['name']; ?></div>
                                        </div>
                                        <?php if ($row_book['book_type'] == 'pdf') { ?>
                                            <a href="chapter.php?bid=<?php echo $row_book['id']; ?>">
                                                <img id="book<?php echo $row_book['id']; ?>" src="img/cover.png" />
                                            </a>
                                        <?php } else { ?>
                                            <a href="slider.php?bid=<?php echo $row_book['id']; ?>">
                                                <img id="book<?php echo $row_book['id']; ?>" src="upload/<?php echo $row_book['name']; ?>/1.jpg" />
                                            </a>
                                            <?php
                                            $j++;
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                                <div class="booklabel_container">
                                    <div class="book_label_img">
                                        <img src="assest/images/my-20corner-20slices/books lable.png" />
                                    </div>
                                    <div class="book_category_container">
                                        <div class="book_category_label">
                                            <?php
                                            $cat_query = "SELECT * FROM type WHERE id='" . $cat . "'";
                                            $cat_result = mysqli_query($con, $cat_query);
                                            $cat_row = $cat_result->fetch_assoc();
                                            ?>
                                            <a href="search.php?cat=<?php echo $cat_row['id']; ?>"><?php echo $cat_row['type']; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>

            </div>
            <?php include('dock.php'); ?>
        </div>
        <?php include('bottom.php'); ?>
    </body>
</html>

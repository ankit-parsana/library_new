<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="index.php">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-clipboard"></i>
                    <span>PDF Book</span>
                </a>
                <ul class="sub">
                    <li><a href="pdf.php">Add New PDF Book</a></li>
<!--                    <li><a href="multipdf.php">Multiple PDF Book</a></li>-->
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-clipboard"></i>
                    <span>Picture Book</span>
                </a>
                <ul class="sub">
                    <li><a href="photo.php">Add New Picture Book</a></li>
<!--                    <li><a href="multiphoto.php">Multiple Photo Book</a></li>-->
                </ul>
            </li>
            <li>
                <a href="all_book.php">
                    <i class="fa fa-book"></i>
                    <span>View/Edit Book</span>
                </a>
            </li>
<!--            <li>
                <a href="asign.php">
                    <i class="fa fa-check"></i>
                    <span>Asign Book to dashboard</span>
                </a>
            </li>-->
            <li>
                <a href="type.php">
                    <i class="fa fa-edit"></i>
                    <span>Book Type</span>
                </a>
            </li>
            <li>
                <a href="ages.php">
                    <i class="fa fa-group"></i>
                    <span>Ages</span>
                </a>
            </li>
            <li>
                <a href="standard.php">
                    <i class="fa fa-pencil"></i>
                    <span>Standard</span>
                </a>
            </li>
            <li>
                <a href="subject.php">
                    <i class="fa fa-folder"></i>
                    <span>Subject</span>
                </a>
            </li>
            <li>
                <a href="profile.php">
                    <i class="fa fa-th-large"></i>
                    <span>School Profile</span>
                </a>
            </li>
            <li>
                <a href="setting.php">
                    <i class="fa fa-cogs"></i>
                    <span>Settings</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
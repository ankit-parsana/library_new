<?php
include('session.php');

$query_select = "SELECT * FROM setting WHERE id=1";
$result_select = mysqli_query($con, $query_select);
$row_select = $result_select->fetch_assoc();

$uname = $row_select['username'];
$password = $row_select['password'];
$standard = $row_select['standard'];
$main_search = $row_select['main_search'];
$search = $row_select['search'];
$bottom = $row_select['bottom'];

if (isset($_POST['submit'])) {
    $uname = $_POST['uname'];
    $password = $_POST['password'];
    $std = $_POST['std'];
    $searchbar = $_POST['searchbar'];
    $subsearch = $_POST['subsearch'];
    $bmenu = $_POST['bmenu'];

    $query_update = "UPDATE setting SET username='" . $uname . "',password='" . $password . "',standard='" . $std . "',main_search='" . $searchbar . "',search='" . $subsearch . "',bottom='" . $bmenu . "' WHERE id=1";
    mysqli_query($con, $query_update);
    echo "<script type='text/javascript'>alert('Detail Update successfully')
	window.location.href='setting.php';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Setting</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Settings
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal" id="setting" method="post" action="">
                                            <div class="form-group ">
                                                <label for="uname" class="control-label col-lg-3">User Name</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="uname" name="uname" type="text" value="<?php echo $uname; ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="password" class="control-label col-lg-3">Password</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="password" name="password" type="password" value="<?php echo $password; ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="std" class="control-label col-lg-3">Select Standard</label>
                                                <div class="col-lg-6">
        <!--                                            <select name="std" class="form-control">
                                                        <option value="<?php echo $standard; ?>"><?php echo $standard; ?></option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                    </select>-->
                                                    <?php
                                                    $query_std = "SELECT * FROM standard ORDER BY id";
                                                    $result_std = mysqli_query($con, $query_std);
                                                    while ($row_std = $result_std->fetch_assoc()) {
                                                        ?>
                                                        <label for="std" class="control-label col-lg-1"><?php echo $row_std['standard']; ?></label>
                                                        <div class="col-lg-3" style=" margin-bottom: 5px; ">
                                                    
                                                            <input type="checkbox" <?php if ($row_std['search_display'] == 'y') { ?> checked <?php } ?> data-on="success" data-off="warning" value="1" name="searchbar">
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="searchbar" class="control-label col-lg-3">Search bar</label>
                                                <div class="col-lg-6">
                                                    <input type="checkbox" <?php if ($main_search == 1) { ?> checked <?php } ?> data-on="success" data-off="warning" value="1" name="searchbar">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="subsearch" class="control-label col-lg-3">Sub Search bars</label>
                                                <div class="col-lg-6">
                                                    <input type="checkbox" <?php if ($search == 1) { ?> checked <?php } ?> data-on="success" data-off="warning" value="1" name="subsearch">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="bmenu" class="control-label col-lg-3">Bottom Menu</label>
                                                <div class="controls col-lg-6">                                            
                                                    <input type="checkbox" <?php if ($bottom == 1) { ?> checked <?php } ?> data-on="success" data-off="warning" value="1" name="bmenu">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <?php include('bottom.php'); ?>

    </body>
</html>
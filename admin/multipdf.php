<?php
include('session.php');

$query_type = "SELECT * FROM type ORDER BY type ASC";
$result_type = mysqli_query($con, $query_type);

$query_std = "SELECT * FROM standard";
$result_std = mysqli_query($con, $query_std);

$query_sub = "SELECT * FROM subject ORDER BY subject ASC";
$result_sub = mysqli_query($con, $query_sub);

$query_age = "SELECT * FROM age";
$result_age = mysqli_query($con, $query_age);

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $type = $_POST['type'];
    $age = $_POST['age'];
    $std = $_POST['std'];
    $sub = $_POST['sub'];
//    $pdf = $_POST['pdf'];
    $total = $_POST['total'];


    $query_insert = "INSERT INTO book(name,type,age,standard,subject,pdf_url,page_no,book_type) VALUES('" . $name . "','" . $type . "','" . $age . "','" . $std . "','" . $sub . "','','" . $total . "','pdf')";
    mysqli_query($con, $query_insert);
    echo "<script type='text/javascript'>alert('Book add successfully.')
	window.location.href='pdf.php';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Add New PDF Book</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-4">
                            <section class="panel">
                                <header class="panel-heading">
                                    Add New PDF Book
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal" id="pdf_form1" method="post" action="">
                                            <div class="form-group ">
                                                <div class="col-lg-12">
                                                    <label for="name1" class="control-label col-lg-12">Book Name</label>
                                                    <input class=" form-control" id="name1" name="name1" type="text" />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="col-lg-12">
                                                    <label for="type1" class="control-label col-lg-12">Select Type</label>
                                                    <select name="type1" class="form-control" id="type1">
                                                        <option value="">All Book Type</option>
                                                        <?php while ($row_type = $result_type->fetch_assoc()) { ?>
                                                            <option value="<?php echo $row_type['type']; ?>"><?php echo $row_type['type']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="col-lg-12">
                                                    <label for="age1" class="control-label col-lg-12">Select Ages</label>
                                                    <select name="age1" class="form-control" id="age1">
                                                        <option value="">All Ages</option>
                                                        <?php while ($row_age = $result_age->fetch_assoc()) { ?>
                                                            <option value="<?php echo $row_age['age']; ?>"><?php echo $row_age['age']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="col-lg-12">
                                                    <label for="std1" class="control-label col-lg-12">Select Standard</label>
                                                    <select name="std1" class="form-control" id="std1">
                                                        <option value="">All Standard</option>
                                                        <?php while ($row_std = $result_std->fetch_assoc()) { ?>
                                                            <option value="<?php echo $row_std['standard']; ?>"><?php echo $row_std['standard']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="col-lg-12">
                                                    <label for="sub1" class="control-label col-lg-12">Select Subject</label>
                                                    <select name="sub1" class="form-control" id="sub1">
                                                        <option value="">All Subject</option>
                                                        <?php while ($row_sub = $result_sub->fetch_assoc()) { ?>
                                                            <option value="<?php echo $row_sub['subject']; ?>"><?php echo $row_sub['subject']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="controls col-lg-12">
                                                    <label for="total1" class="control-label col-lg-12">Total No. of Chapter</label>
                                                    <input class=" form-control" id="total1" name="total1" type="text" />
                                                </div>
                                            </div>
                                            <!--                                            <div class="form-group ">
                                                                                            <label for="pdf" class="control-label col-lg-3">Upload PDF</label>
                                                                                            <div class="controls col-lg-6">                                            
                                                                                                <input type="file" class="default" name="pdf" />
                                                                                            </div>
                                                                                        </div>-->
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <button class="btn btn-primary submit" id="submit1" name="submit1" type="submit">Save</button>
                                                    <span class="error" style="display:none"> Please Enter Valid Data</span>
                                                    <span class="success" style="display:none"> Registration Successfully</span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>


                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
        <script type="text/javascript" >
            $(function() {
                $(".submit").click(function() {
                    var name = $("#name1").val();
                    var type = $("#type1").val();
                    var age = $("#age1").val();
                    var std = $("#std1").val();
                    var sub = $("#sub1").val();
                    var num = $("#total1").val();
                    var dataString = 'name=' + name + '&type=' + type + '&age=' + age + '&std=' + std + '&sub=' + sub + '&num=' + num;

                    if (name == '' || type == '' || age == '' || std == '' || sub == '' || num == '')
                    {
                        $('.success').fadeOut(200).hide();
                        $('.error').fadeOut(200).show();
                    }
                    else
                    {
                        $.ajax({
                            type: "POST",
                            url: "mfrom1.php",
                            data: dataString,
                            success: function() {
                                $('.success').fadeIn(200).show();
                                $('.error').fadeOut(200).hide();
                                alert('yes');
                                //                                $('#name').val('');
                                //                                $('#age').val('');
                                //                                $('#email').val('');
                                //                                $('#city').val('');
                                //                                $('#phone').val('');
                                //                                $('#date').val('');
                            }
                        });
                    }
                    return false;
                });
            });
        </script>


    </body>
</html>
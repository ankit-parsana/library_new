<?php
include('session.php');
$book_name = $_GET['bname'];

$query_select_id = "SELECT * FROM book WHERE name='" . $book_name . "'";
$rs_select_id = mysqli_query($con, $query_select_id);
$row_select_id = $rs_select_id->fetch_assoc();
$book_id = $row_select_id['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Upload Book PDF</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Upload Book PDF
                                    <span class="pull-right" style=" margin-top: -7px; ">
                                        <a class="btn btn-success" href="all_book.php"><i class="fa fa-book mr8"></i>View Book</a>
                                        <a class="btn btn-info" href="photo.php"><i class="fa fa-plus mr8"></i>Add New Photo Book</a>
                                        <a class="btn btn-danger" href="pdf.php"><i class="fa fa-plus mr8"></i>Add New PDF Book</a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form id="imageform" method="post" enctype="multipart/form-data" action='ajaxpdfUpload.php?bid=<?php echo $book_id; ?>&bname=<?php echo $book_name; ?>' style="clear:both">
                                            <div id='imageloadbutton'>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <span class="btn btn-lg btn-primary btn-file">
                                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select Book PDF</span>
                                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Select more PDF</span>
                                                        <input type="file" name="photos[]" id="photoimg" multiple="true" class="default" />
                                                    </span>
                                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                </div>
<!--                                                <input type="file" name="photos[]" id="photoimg" multiple="true" class="default" />-->
                                            </div>
                                            <div id='imageloadstatus' style='display:none'><img src="images/loader.gif" alt="Uploading...."/></div>
                                        </form>
                                        <div id='preview'></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>
        <script src="js/lib/jquery.js"></script>
        <script src="js/jquery.min.js"></script>

        <!-- Placed js at the end of the document so the pages load faster -->
        <!--Core js-->

        <script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
        <script src="bs3/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="js/jquery-validate/jquery.validate.min.js"></script>
        <script src="js/jquery-validate/validation-init.js"></script>

        <script src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
        <script src="js/scrollTo/jquery.scrollTo.min.js"></script>
        <script src="js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>

        <!--clock init-->
        <script src="assets/css3clock/js/script.js"></script>
        <script src="assets/bootstrap-switch-master/build/js/bootstrap-switch.js"></script>

        <script type="text/javascript" src="assets/fuelux/js/spinner.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
        <script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
        <script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
        <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-daterangepicker/moment.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
        <script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="assets/jquery-tags-input/jquery.tagsinput.js"></script>
        <!--<script src="js/custom-select/jquery.customSelect.min.js" ></script>-->
        <script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>

        <script src="js/scripts.js"></script>
        <script>
            $(document).ready(function() {
                $("#category1").change(function() {
                    var category1 = $("#category1").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category1=" + category1,
                        success: function(data) {
                            $("#input1_1").html(data);
                            $("#input1_2").html(data);
                            $("#input1_3").html(data);
                        }
                    });
                });
                $("#category2").change(function() {
                    var category2 = $("#category2").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category2=" + category2,
                        success: function(data) {
                            $("#input2_1").html(data);
                            $("#input2_2").html(data);
                            $("#input2_3").html(data);
                        }
                    });
                });
                $("#category3").change(function() {
                    var category3 = $("#category3").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category3=" + category3,
                        success: function(data) {
                            $("#input3_1").html(data);
                            $("#input3_2").html(data);
                            $("#input3_3").html(data);
                        }
                    });
                });
                $("#category4").change(function() {
                    var category4 = $("#category4").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category4=" + category4,
                        success: function(data) {
                            $("#input4_1").html(data);
                            $("#input4_2").html(data);
                            $("#input4_3").html(data);
                        }
                    });
                });
                $("#category5").change(function() {
                    var category5 = $("#category5").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category5=" + category5,
                        success: function(data) {
                            $("#input5_1").html(data);
                            $("#input5_2").html(data);
                            $("#input5_3").html(data);
                        }
                    });
                });
                $("#category6").change(function() {
                    var category6 = $("#category6").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category6=" + category6,
                        success: function(data) {
                            $("#input6_1").html(data);
                            $("#input6_2").html(data);
                            $("#input6_3").html(data);
                        }
                    });
                });
                $("#category7").change(function() {
                    var category7 = $("#category7").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category7=" + category7,
                        success: function(data) {
                            $("#input7_1").html(data);
                            $("#input7_2").html(data);
                            $("#input7_3").html(data);
                        }
                    });
                });
                $("#category8").change(function() {
                    var category8 = $("#category8").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category8=" + category8,
                        success: function(data) {
                            $("#input8_1").html(data);
                            $("#input8_2").html(data);
                            $("#input8_3").html(data);
                        }
                    });
                });
                $("#category9").change(function() {
                    var category9 = $("#category9").val();
                    $.ajax({
                        type: "post",
                        url: "getbook.php",
                        data: "category9=" + category9,
                        success: function(data) {
                            $("#input9_1").html(data);
                            $("#input9_2").html(data);
                            $("#input9_3").html(data);
                        }
                    });
                });
            });
        </script>
        <script src="js/editable-table/table-editable.js"></script>

        <script src="js/toggle-button/toggle-init.js"></script>
        <script src="js/advanced-form/advanced-form.js"></script>
        <!--script for this page-->

        <script>
            jQuery(document).ready(function() {
                EditableTable.init();
            });
        </script>
        <script src="js/jquery.wallform.js"></script>
        <script>
            $(document).ready(function() {

                $('#photoimg').die('click').live('change', function() {
                    //$("#preview").html('');

                    $("#imageform").ajaxForm({target: '#preview',
                        beforeSubmit: function() {

                            console.log('ttest');
                            $("#imageloadstatus").show();
                            $("#imageloadbutton").hide();
                        },
                        success: function() {
                            console.log('test');
                            $("#imageloadstatus").hide();
                            $("#imageloadbutton").show();
                        },
                        error: function() {
                            console.log('xtest');
                            $("#imageloadstatus").hide();
                            $("#imageloadbutton").show();
                        }}).submit();
                });
            });
        </script>
    </body>
</html>
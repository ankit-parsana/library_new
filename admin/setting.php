<?php
include('session.php');

if (isset($_POST['submit'])) {  //echo"<pre>"; print_r($_POST);    print_r($_POST['standard']);
    
    /* update standard */
    $query_std_u = "UPDATE standard SET search_display='n'";
    mysqli_query($con, $query_std_u);
    if (count($_POST['standard']) > 0) {
        $str = implode(",", $_POST['standard']);
        $query = "UPDATE standard SET search_display='y' WHERE id IN (" . $str . ")";
        mysqli_query($con, $query);
    }
    
    /* update age */
    $query_age_u = "UPDATE age SET search_display='n'";
    mysqli_query($con, $query_age_u);
    if (count($_POST['age']) > 0) {
        $str = implode(",", $_POST['age']);
        $query = "UPDATE age SET search_display='y' WHERE id IN (" . $str . ")";
        mysqli_query($con, $query);
    }
    
    /* update age */
    $query_sub_u = "UPDATE subject SET search_display='n'";
    mysqli_query($con, $query_sub_u);
    if (count($_POST['subject']) > 0) {
        $str = implode(",", $_POST['subject']);
        $query = "UPDATE subject SET search_display='y' WHERE id IN (" . $str . ")";
        mysqli_query($con, $query);
    }
    
    /* update type */
    $query_type_u = "UPDATE type SET search_display='n'";
    mysqli_query($con, $query_type_u);
    if (count($_POST['type']) > 0) {
        $str = implode(",", $_POST['type']);
        $query = "UPDATE type SET search_display='y' WHERE id IN (" . $str . ")";
        mysqli_query($con, $query);
    }

    $uname = $_POST['uname'];
    $password = $_POST['password'];
    //$std = $_POST['std'];
    $searchbar = $_POST['searchbar'];
    $subsearch = $_POST['subsearch'];
    $bmenu = $_POST['bmenu'];

    $query_update = "UPDATE setting SET username='" . $uname . "',password='" . $password . "',standard='" . $std . "',main_search='" . $searchbar . "',search='" . $subsearch . "',bottom='" . $bmenu . "' WHERE id=1";
    mysqli_query($con, $query_update);
    echo "<script type='text/javascript'>alert('Detail Update successfully')
	window.location.href='setting.php';</script>";
}

$query_select = "SELECT * FROM setting WHERE id=1";
$result_select = mysqli_query($con, $query_select);
$row_select = $result_select->fetch_assoc();

$uname = $row_select['username'];
$password = $row_select['password'];
$standard = $row_select['standard'];
$main_search = $row_select['main_search'];
$search = $row_select['search'];
$bottom = $row_select['bottom'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Setting</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Settings
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal" id="setting" method="post" action="">
                                            <div class="form-group ">
                                                <label for="uname" class="control-label col-lg-3">User Name</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="uname" name="uname" type="text" value="<?php echo $uname; ?>" />
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group ">
                                                <label for="password" class="control-label col-lg-3">Password</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="password" name="password" type="password" value="<?php echo $password; ?>" />
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group ">
                                                <label for="std" class="control-label col-lg-3">Select Standard</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $query_std = "SELECT * FROM standard ORDER BY id";
                                                    $result_std = mysqli_query($con, $query_std);
                                                    while ($row_std = $result_std->fetch_assoc()) {
                                                        ?>
                                                        <label for="std" class="control-label col-lg-1"><?php echo $row_std['standard']; ?></label>
                                                        <div class="col-lg-3" style=" margin-bottom: 5px; ">
                                                            <input type="checkbox" <?php if ($row_std['search_display'] == 'y') { ?> checked <?php } ?> data-on="success" data-off="warning" value="<?php echo $row_std['standard']; ?>" name="<?php echo "standard[" . $row_std['id'] . "]"; ?>">
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group ">
                                                <label for="std" class="control-label col-lg-3">Select Age</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $query_age = "SELECT * FROM age ORDER BY id";
                                                    $result_age = mysqli_query($con, $query_age);
                                                    while ($row_age = $result_age->fetch_assoc()) {
                                                        ?>
                                                        <label for="age" class="control-label col-lg-1"><?php echo $row_age['age']; ?></label>
                                                        <div class="col-lg-3" style=" margin-bottom: 5px; ">
                                                            <input type="checkbox" <?php if ($row_age['search_display'] == 'y') { ?> checked <?php } ?> data-on="info" data-off="primary" value="<?php echo $row_age['id']; ?>" name="<?php echo "age[" . $row_age['id'] . "]"; ?>">
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group ">
                                                <label for="sub" class="control-label col-lg-3">Select Subject</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $query_sub = "SELECT * FROM subject ORDER BY id";
                                                    $result_sub = mysqli_query($con, $query_sub);
                                                    while ($row_sub = $result_sub->fetch_assoc()) {
                                                        ?>
                                                        <label for="subject" class="control-label col-lg-3"><?php echo $row_sub['subject']; ?></label>
                                                        <div class="col-lg-3" style=" margin-bottom: 5px; ">
                                                            <input type="checkbox" <?php if ($row_sub['search_display'] == 'y') { ?> checked <?php } ?> data-on="danger" data-off="default" value="<?php echo $row_sub['id']; ?>" name="<?php echo "subject[" . $row_sub['id'] . "]"; ?>">
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group ">
                                                <label for="type" class="control-label col-lg-3">Select Type</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $query_type = "SELECT * FROM type ORDER BY id";
                                                    $result_type = mysqli_query($con, $query_type);
                                                    while ($row_type = $result_type->fetch_assoc()) {
                                                        ?>
                                                        <label for="type" class="control-label col-lg-3"><?php echo $row_type['type']; ?></label>
                                                        <div class="col-lg-3" style=" margin-bottom: 5px; ">
                                                            <input type="checkbox" <?php if ($row_type['search_display'] == 'y') { ?> checked <?php } ?> data-on="warning" data-off="success" value="<?php echo $row_type['id']; ?>" name="<?php echo "type[" . $row_type['id'] . "]"; ?>">
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group ">
                                                <label for="searchbar" class="control-label col-lg-3">Search bar</label>
                                                <div class="col-lg-6">
                                                    <input type="checkbox" <?php if ($main_search == 1) { ?> checked <?php } ?> data-on="success" data-off="warning" value="1" name="searchbar">
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group ">
                                                <label for="subsearch" class="control-label col-lg-3">Sub Search bars</label>
                                                <div class="col-lg-6">
                                                    <input type="checkbox" <?php if ($search == 1) { ?> checked <?php } ?> data-on="success" data-off="warning" value="1" name="subsearch">
                                                </div>
                                            </div>
                                            <hr>
<!--                                            <div class="form-group ">
                                                <label for="bmenu" class="control-label col-lg-3">Bottom Menu</label>
                                                <div class="controls col-lg-6">                                            
                                                    <input type="checkbox" <?php if ($bottom == 1) { ?> checked <?php } ?> data-on="success" data-off="warning" value="1" name="bmenu">
                                                </div>
                                            </div>-->
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <?php include('bottom.php'); ?>

    </body>
</html>
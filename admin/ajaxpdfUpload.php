<?php

error_reporting(0);
include('session.php');
$book_id = $_GET['bid'];
$book_name = $_GET['bname'];

//$session_id = '1'; //$session id
define("MAX_SIZE", "180000000");

function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}

$valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "pdf");
if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

    $uploaddir = "../upload"; //a directory inside
    $uploaddir = $uploaddir."/".$book_name."/";
    if (is_dir($uploaddir) == false) {
        mkdir("$uploaddir", 0700);  // Create directory if it does not exist
    }
    foreach ($_FILES['photos']['name'] as $name => $value) {

        $filename = stripslashes($_FILES['photos']['name'][$name]);
        $size = filesize($_FILES['photos']['tmp_name'][$name]);
        //get the extension of the file in a lower case format
        $ext = getExtension($filename);
        $ext = strtolower($ext);

        if (in_array($ext, $valid_formats)) {
            if ($size < (MAX_SIZE * 1024)) {
                $image_name = $filename;
                echo "<div class='col-md-2 prev-wrapper'><img src='../img/cover.png' class='imgList'><br><h3>$image_name</h3></div>";
                $newname = $uploaddir . $image_name;

                if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname)) {
                    $time = time();
                    mysqli_query($con,"INSERT INTO user_uploads(image_name,user_id,created) VALUES('$image_name','$book_id','$time')");
                } else {
                    echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
                }
            } else {
                echo '<span class="imgList">You have exceeded the size limit!</span>';
            }
        } else {
            echo '<span class="imgList">Unknown extension!</span>';
        }
    }
}
?>
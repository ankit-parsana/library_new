<?php
include('session.php');
$book_name = $_GET['bname'];

$query_select_id = "SELECT * FROM book WHERE name='" . $book_name . "'";
$rs_select_id = mysqli_query($con, $query_select_id);
$row_select_id = $rs_select_id->fetch_assoc();
$book_id = $row_select_id['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Upload Book images</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Upload Book images
                                    <span class="pull-right" style=" margin-top: -7px; ">
                                        <a class="btn btn-success" href="all_book.php"><i class="fa fa-book mr8"></i>View Book</a>
                                        <a class="btn btn-info" href="photo.php"><i class="fa fa-plus mr8"></i>Add New Photo Book</a>
                                        <a class="btn btn-danger" href="pdf.php"><i class="fa fa-plus mr8"></i>Add New PDF Book</a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form id="imageform" method="post" enctype="multipart/form-data" action='ajaxImageUpload.php?bid=<?php echo $book_id; ?>&bname=<?php echo $book_name; ?>' style="clear:both">
                                            <div id='imageloadbutton'>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <span class="btn btn-lg btn-primary btn-file">
                                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select Book Image</span>
                                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Select more Images</span>
                                                        <input type="file" name="photos[]" id="photoimg" multiple="true" class="default" />
                                                    </span>
                                                    <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                </div>
<!--                                                <input type="file" name="photos[]" id="photoimg" multiple="true" class="default" />-->
                                            </div>
                                            <div id='imageloadstatus' style='display:none'><img src="images/loader.gif" alt="Uploading...."/></div>
                                        </form>
                                        <div id='preview'></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>
        <script src="js/jquery.min.js"></script>

        <script src="js/jquery.wallform.js"></script>
        <script>
            $(document).ready(function() {
                $('#photoimg').die('click').live('change', function() {
                    //$("#preview").html('');
                    $("#imageform").ajaxForm({target: '#preview',
                        beforeSubmit: function() {
                            console.log('ttest');
                            $("#imageloadstatus").show();
                            $("#imageloadbutton").hide();
                        },
                        success: function() {
                            console.log('test');
                            $("#imageloadstatus").hide();
                            $("#imageloadbutton").show();
                        },
                        error: function() {
                            console.log('xtest');
                            $("#imageloadstatus").hide();
                            $("#imageloadbutton").show();
                        }}).submit();
                });
            });
        </script>
    </body>
</html>
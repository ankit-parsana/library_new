<?php
include('session.php');

$query_type = "SELECT * FROM type";
$result_type= mysqli_query($con, $query_type);
while ($row_select = $result_type->fetch_assoc()) {
    $id = "0".$row_select['id'];
    $type[$id] = $row_select['type'];
}

$query_age = "SELECT * FROM age";
$result_age= mysqli_query($con, $query_age);
while ($row_select = $result_age->fetch_assoc()) {
    $id = "0".$row_select['id'];
    $age[$id] = $row_select['age'];
}

$query_standard = "SELECT * FROM standard";
$result_standard= mysqli_query($con, $query_standard);
while ($row_select = $result_standard->fetch_assoc()) {
    $id = "0".$row_select['id'];
    $standard[$id] = $row_select['standard'];
}

$query_subject = "SELECT * FROM subject";
$result_subject = mysqli_query($con, $query_subject);
while ($row_select = $result_subject->fetch_assoc()) {
    $id = "0".$row_select['id'];
    $subject[$id] = $row_select['subject'];
}


$query_select = "SELECT * FROM book ORDER BY name ASC";
$result_select = mysqli_query($con, $query_select);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Book Type</title>
        <?php include('head.php'); ?>
    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    View/Edit Book Details
                                    <span class="tools pull-right">
                                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                                        <a href="javascript:;" class="fa fa-cog"></a>
                                        <a href="javascript:;" class="fa fa-times"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="adv-table editable-table ">
                                        <div class="clearfix">
                                            <div class="btn-group">
                                                <button id="editable-sample_new" class="btn btn-primary">
                                                    Add New <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="space15"></div>
                                        <table class="table table-striped table-hover table-bordered" id="editable-sample">
                                            <thead>
                                                <tr>
                                                    <th>Sr no</th>
                                                    <th>Book Name</th>
                                                    <th>Book Type</th>
                                                    <th>Ages</th>
                                                    <th>Standard</th>
                                                    <th>Subject</th>
<!--                                                    <th>PDF URL</th>-->
<!--                                                    <th>No. of book page/Chapter</th>-->
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $j = 1;
                                                while ($row_select = $result_select->fetch_assoc()) {
                                                    ?>
                                                    <tr class="">
                                                        <td><?php echo $j; ?></td>
                                                        <td><?php echo $row_select['name']; ?></td>
                                                        <?php 
                                                        $type_sep = explode(",", rtrim($row_select['type'], ","));
                                                        $str = "";
                                                        foreach($type_sep as $row)
                                                        {
                                                            $str.= $type[$row].' , '; 
                                                        }                                                        
                                                        ?>
                                                        <td><?php echo rtrim($str, " , "); ?></td>
                                                        <?php 
                                                        $age_sep = explode(",", rtrim($row_select['age'], ","));
                                                        $str_age = "";
                                                        foreach($age_sep as $row)
                                                        {
                                                           $str_age.= $age[$row].' , '; 
                                                        }                                                        
                                                        ?>
                                                        <td><?php echo rtrim($str_age, " , "); ?></td>
                                                        <?php $age_standard = explode(",", rtrim($row_select['standard'], ","));
                                                        $str_standard = "";
                                                        foreach($age_standard as $row)
                                                        {
                                                           $str_standard.= $standard[$row].' , '; 
                                                        }                                                        
                                                        ?>
                                                        <td><?php echo rtrim($str_standard, " , "); ?></td>
                                                        <?php $age_sub = explode(",", rtrim($row_select['subject'], ","));
                                                        $str_sub = "";
                                                        foreach($age_sub as $row)
                                                        {
                                                           $str_sub.= $subject[$row].' , '; 
                                                        }                                                        
                                                        ?>
                                                        <td><?php echo rtrim($str_sub, " , "); ?></td>
                                                        
<!--                                                        <td><?php echo $row_select['page_no']; ?></td>-->
                                                        <td class="ta-center">
                                                            <a class="edit btn btn-info btn-sm" href="edit_book.php?id=<?php echo $row_select['id']; ?>&t=<?php echo $row_select['book_type']; ?>"><i class="fa fa-pencil"></i>Edit</a>
                                                        </td>
                                                        <td class="ta-center">
                                                            <form method="post" action="delete_book.php" name="form_del">
                                                                <button type="submit" name="delete" class="delete btn btn-danger btn-sm" id="delete"><i class="fa fa-times"></i>Delete</button>
                                                                <input type="hidden" name="hidden_del" value="<?php echo $row_select['id']; ?>">
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $j++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <script src="js/lib/jquery-1.8.3.min.js"></script>
        <script src="bs3/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
        <script src="js/scrollTo/jquery.scrollTo.min.js"></script>
        <script src="js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>

        <script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>

        <!--common script init for all pages-->
        <script src="js/scripts.js"></script>

        <!--script for this page only-->
        <script src="js/editable-table/table-editable.js"></script>

        <!-- END JAVASCRIPTS -->
        <script>
            jQuery(document).ready(function() {
                EditableTable.init();
            });
        </script>
    </body>
</html>
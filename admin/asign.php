<?php
include('session.php');

$query_cat = "SELECT * FROM type";
$result_cat = mysqli_query($con, $query_cat);
while ($row_cat = $result_cat->fetch_assoc()) {
    //echo "<pre>".  print_r($row_cat);
    $cat[] = $row_cat['type'];
}
if(isset($_POST['sumbit'])) {
//    $query_asign = "UPDATE asign_book SET cat1='".$_POST['category1']."', cat2='".$_POST['category2']."', cat3='".$_POST['category3']."', cat4='".$_POST['category4']."', cat5='".$_POST['category5']."', cat6='".$_POST['category6']."', cat7='".$_POST['category7']."', cat8='".$_POST['category8']."', cat9='".$_POST['category9']."', row_1_1='".$_POST['input1_1']."', row_1_2='".$_POST['input1_2']."', row_1_3='".$_POST['input1_3']."', row_1_4='".$_POST['input2_1']."', row_1_5='".$_POST['input2_2']."', row_1_6='".$_POST['input2_3']."', row_1_7='".$_POST['input3_1']."', row_1_8='".$_POST['input3_2']."', row_1_9='".$_POST['input3_3']."',row_2_1='".$_POST['input4_1']."', row_2_2='".$_POST['input4_2']."', row_2_3='".$_POST['input4_3']."', row_2_4='".$_POST['input5_1']."', row_2_5='".$_POST['input5_2']."', row_2_6='".$_POST['input5_3']."', row_2_7='".$_POST['input6_1']."', row_2_8='".$_POST['input6_2']."', row_2_9='".$_POST['input6_3']."', row_3_1='".$_POST['input7_1']."', row_3_2='".$_POST['input7_2']."', row_3_3='".$_POST['input7_3']."', row_3_4='".$_POST['input8_1']."', row_3_5='".$_POST['input8_2']."', row_3_6='".$_POST['input8_3']."', row_3_7='".$_POST['input9_1']."', row_3_8='".$_POST['input9_2']."', row_3_9='".$_POST['input9_3']."', ";
//    mysqli_query($con,$query_asign);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Asign book to dashboard</title>
        <?php include('head.php'); ?>
    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <form name="asign" method="post" action="">
                        <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Asign book to Dashboard - Row 1
                                        <span class="tools pull-right">
                                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                                            <a class="fa fa-times" href="javascript:;"></a>
                                        </span>
                                    </header>
                                    <div class="panel-body">
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 1
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="category1" class="control-label">category</label>
                                                    <select name="category1" id="category1" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input1_1" class="control-label">Select Book 1</label>
                                                    <select name="input1_1" id="input1_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input1_2" class="control-label">Select Book 2</label>
                                                    <select name="input1_2" id="input1_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input1_3" class="control-label">Select Book 3</label>
                                                    <select name="input1_3" id="input1_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 2
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="category2" class="control-label">category</label>
                                                    <select name="category2" id="category2" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input2_1" class="control-label">Select Book 1</label>
                                                    <select name="input2_1" id="input2_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input2_2" class="control-label">Select Book 2</label>
                                                    <select name="input2_2" id="input2_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input2_3" class="control-label">Select Book 3</label>
                                                    <select name="input2_3" id="input2_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 3
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="category3" class="control-label">category</label>
                                                    <select name="category3" id="category3" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input3_1" class="control-label">Select Book 1</label>
                                                    <select name="input3_1"  id="input3_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input3_2" class="control-label">Select Book 2</label>
                                                    <select name="input3_2"  id="input3_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input3_3" class="control-label">Select Book 3</label>
                                                    <select name="input3_3" id="input3_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Asign book to Dashboard - Row 2
                                        <span class="tools pull-right">
                                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                                            <a class="fa fa-times" href="javascript:;"></a>
                                        </span>
                                    </header>
                                    <div class="panel-body">
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="category4" class="control-label">category</label>
                                                    <select name="category4" id="category4" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input4_1" class="control-label">Select Book 1</label>
                                                    <select name="input4_1" id="input4_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input4_2" class="control-label">Select Book 2</label>
                                                    <select name="input4_2"  id="input4_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input4_3" class="control-label">Select Book 3</label>
                                                    <select name="input4_3" id="input4_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 2
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="category5" class="control-label">category</label>
                                                    <select name="category5" id="category5" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input5_1" class="control-label">Select Book 1</label>
                                                    <select name="input5_1" id="input5_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input5_2" class="control-label">Select Book 2</label>
                                                    <select name="input5_2" id="input5_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input5_3" class="control-label">Select Book 3</label>
                                                    <select name="input5_3" id="input5_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 3
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="category6" class="control-label">category</label>
                                                    <select name="category6" id="category6" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input6_1" class="control-label">Select Book 1</label>
                                                    <select name="input6_1" id="input6_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input6_2" class="control-label">Select Book 2</label>
                                                    <select name="input6_2" id="input6_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input6_3" class="control-label">Select Book 3</label>
                                                    <select name="input6_3" id="input6_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Asign book to Dashboard - Row 3
                                        <span class="tools pull-right">
                                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                                            <a class="fa fa-times" href="javascript:;"></a>
                                        </span>
                                    </header>
                                    <div class="panel-body">
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="category7" class="control-label">category</label>
                                                    <select name="category7" id="category7" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input7_1" class="control-label">Select Book 1</label>
                                                    <select name="input7_1" id="input7_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input7_2" class="control-label">Select Book 2</label>
                                                    <select name="input7_2" id="input7_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input7_3" class="control-label">Select Book 3</label>
                                                    <select name="input7_3" id="input7_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 2
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group ">
                                                    <label for="category8" class="control-label">Select category</label>
                                                    <select name="category8" id="category8" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input8_1" class="control-label">Select Book 1</label>
                                                    <select name="input8_1" id="input8_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input8_2" class="control-label">Select Book 2</label>
                                                    <select name="input8_2" id="input8_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input8_3" class="control-label">Select Book 3</label>
                                                    <select name="input8_3" id="input8_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="panel panel-info col-md-4">
                                            <header class="panel-heading">
                                                Column 3
                                            </header>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="category9" class="control-label">Select category</label>
                                                    <select name="category9" id="category9" class="form-control">
                                                        <option value="">Select Category</option>
                                                        <?php foreach ($cat as $cat_print) { ?>
                                                            <option value="<?php echo $cat_print; ?>"><?php echo $cat_print; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input9_1" class="control-label">Select Book 1</label>
                                                    <select name="input9_1" id="input9_1" class="form-control">
                                                        <option value="">Select Book</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="input9_2" class="control-label">Select Book 2</label>
                                                    <select name="input9_2" id="input9_2" class="form-control">
                                                        <option value="">Select Book</option>
                                                        <option value="">Select Book</option>
                                                        <option value="">Select Category</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input9_3" class="control-label">Select Book 3</label>
                                                    <select name="input9_3" id="input9_3" class="form-control">
                                                        <option value="">Select Book</option>
                                                        <option value="">Select Book</option>
                                                        <option value="">Select Category</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-12">
                                <section class="panel">
                                    <div class="panel-body">
                                        <input type="submit" name="submit" value="submit" class="btn btn-primary pull-right">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </form>
                </section>
            </section>
            <!--main content end-->
        </section>
        <?php include('bottom.php'); ?>

    </body>
</html>
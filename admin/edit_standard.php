<?php
include('session.php');
$id = $_GET['id'];
$query_edit = "SELECT * FROM standard WHERE id='".$id."'";
$result_edit = mysqli_query($con,$query_edit);
$row_edit = $result_edit->fetch_assoc();

if (isset($_POST['submit'])) {
    $standard = $_POST['standard'];

    $query_update = "UPDATE standard SET standard='".$standard."' WHERE id='".$id."'"; 
    mysqli_query($con,$query_update);
    echo "<script type='text/javascript'>alert('Standard Updated.')
	window.location.href='standard.php';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Edit Standard</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Edit Standard
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal" id="photo_form" method="post" action="">
                                            <div class="form-group ">
                                                <label for="type" class="control-label col-lg-3">Standard</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="standard" name="standard" type="text" value="<?php echo $row_edit['standard']; ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <script src="js/lib/jquery-1.8.3.min.js"></script>
        <script src="bs3/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
        <script src="js/scrollTo/jquery.scrollTo.min.js"></script>
        <script src="js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>

        <script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>

        <!--common script init for all pages-->
        <script src="js/scripts.js"></script>

        <!--script for this page only-->
        <script src="js/editable-table/table-editable.js"></script>

        <!-- END JAVASCRIPTS -->
        <script>
            jQuery(document).ready(function() {
                EditableTable1.init();
            });
        </script>

    </body>
</html>
<?php
error_reporting(E_ALL ^ E_NOTICE);
// Your MySQL hostname. Usualy named as 'localhost', so you're NOT necessary to change this even this script has already online on the internet.
$hostname = 'localhost';  
// Your database name.
$dbname   = 'library_new'; 
// Your database username.
$username = 'root';
// Your database password. If your database has no password, leave it empty.
$password = '';

// Let's connect to host
$con = mysqli_connect($hostname, $username, $password, $dbname) or DIE('Connection to host is failed, perhaps the service is down!');
// Select the database
//mysqli_select_db($dbname) or DIE('Database name is not available!');
session_start();
?>
<?php
include('session.php');

if (isset($_POST['submit'])) {  //echo"<pre>"; print_r($_POST);    print_r($_POST['standard']);
    $query_update = "UPDATE setting SET school_name='" . $_POST['sname'] . "' WHERE id=1";
    mysqli_query($con, $query_update);
    echo "<script type='text/javascript'>alert('Detail Update successfully')
	window.location.href='profile.php';</script>";
}

if (isset($_POST['upload'])) {
    $allowedExts = array("gif", "jpeg", "jpg", "png");
    $file_name = $_FILES["file"]["name"];
    $array = explode('.', $file_name);
    $extension = end($array);
    if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/x-png") || ($_FILES["file"]["type"] == "image/png")) && ($_FILES["file"]["size"] < 12000000) && in_array($extension, $allowedExts)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
        } else {
            if (file_exists("img/" . $_FILES["file"]["name"])) {
                echo $_FILES["file"]["name"] . " already exists. ";
            } else {
                $pure = $_FILES["file"]["name"];
                $type = $_FILES["file"]["type"];
                move_uploaded_file($_FILES["file"]["tmp_name"], "img/" . $_FILES["file"]["name"]);
                $sql = "UPDATE setting SET logo='" . $pure . "' WHERE id=1";
                $result_image = mysqli_query($con, $sql);
            }
        }
    }
}

$query = "select * from setting where id=1";
$result = mysqli_query($con, $query);
$row = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Profile</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Profile
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal" id="setting" method="post" action="" enctype="multipart/form-data">
                                            <div class="form-group ">
                                                <label for="sname" class="control-label col-lg-3">School Name</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="sname" name="sname" type="text" value="<?php echo $row['school_name']; ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="file" class="control-label col-lg-3">Change Logo</label>
                                                <div class="col-lg-4">
                                                    <input type="file" name="file" id="file">
                                                </div>
                                                <div class="col-lg-3">
                                                    <button name="upload" class="btn btn-info btn-wide" type="submit">Upload</button>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="logo" class="control-label col-lg-3">Logo</label>
                                                <div class="col-lg-6">
                                                    <img src="<?php echo "img/" . $row['logo']; ?>" alt="image" style=" width: 100%; ">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <?php include('bottom.php'); ?>

    </body>
</html>
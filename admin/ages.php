<?php
include('session.php');

if (isset($_POST['submit'])) {
    $age = $_POST['age'];

    $query_insert = "INSERT INTO age(age) VALUES('" . $age . "')";
    mysqli_query($con, $query_insert);
    echo "<script type='text/javascript'>alert('Age add successfully.')
	window.location.href='ages.php';</script>";
}

$query_select = "SELECT * FROM age ORDER BY age ASC";
$result_select = mysqli_query($con, $query_select);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Ages</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-5">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Add Ages
                                        <span class="tools pull-right">
                                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                                            <a class="fa fa-cog" href="javascript:;"></a>
                                            <a class="fa fa-times" href="javascript:;"></a>
                                        </span>
                                    </header>
                                    <div class="panel-body">
                                        <div class="form">
                                            <form class="cmxform form-horizontal" id="photo_form" method="post" action="">
                                                <div class="form-group ">
                                                    <label for="age" class="control-label col-lg-3">Age</label>
                                                    <div class="col-lg-6">
                                                        <input class=" form-control" id="age" name="age" type="text" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-3 col-lg-6">
                                                        <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-7">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Edit/Delete Age
                                        <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-cog"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                        </span>
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table editable-table ">
                                            <div class="clearfix">
                                                <div class="btn-group">
                                                    <button id="editable-sample_new1" class="btn btn-primary">
                                                        Add New <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="space15"></div>
                                            <table class="table table-striped table-hover table-bordered" id="editable-sample1">
                                                <thead>
                                                    <tr>
                                                        <th>Sr No</th>
                                                        <th>Ages</th>
                                                        <th>Edit</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $j = 1;
                                                    while ($row_select = $result_select->fetch_assoc()) {
                                                        ?>
                                                        <tr class="">
                                                            <td><?php echo $j; ?></td>
                                                            <td><?php echo $row_select['age']; ?></td>
                                                            <td class="ta-center">
                                                                <a class="edit btn btn-info btn-sm" href="edit_age.php?id=<?php echo $row_select['id']; ?>"><i class="fa fa-pencil"></i>Edit</a>
                                                            </td>
                                                            <td class="ta-center">
                                                                <form method="post" action="delete.php?chk=age" name="form_del">
                                                                    <button type="submit" name="delete" class="delete btn btn-danger btn-sm" id="delete"><i class="fa fa-times"></i>Delete</button>
                                                                    <input type="hidden" name="hidden_del" value="<?php echo $row_select['id']; ?>">
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $j++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <script src="js/lib/jquery-1.8.3.min.js"></script>
        <script src="bs3/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
        <script src="js/scrollTo/jquery.scrollTo.min.js"></script>
        <script src="js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>

        <script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>

        <!--common script init for all pages-->
        <script src="js/scripts.js"></script>

        <!--script for this page only-->
        <script src="js/editable-table/table-editable.js"></script>

        <!-- END JAVASCRIPTS -->
        <script>
            jQuery(document).ready(function() {
                EditableTable1.init();
            });
        </script>

    </body>
</html>
<?php
include('session.php');

$query = "SELECT * FROM setting WHERE id=1";
$rs = mysqli_query($con, $query);
$row = $rs->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ankit">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Add New PDF Book</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <div class="panel-body ta-center">
                                    <div class="row school_logo">
                                        <div class="col-md-6 col-md-ofsset-3 col-sm-3 col-sm-offset-3">
                                             <img src="img/<?php echo $row['logo']; ?>" alt="school logo" style="width: 100%">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <h1><?php echo $row['school_name']; ?></h1>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <?php include('bottom.php'); ?>

    </body>
</html>
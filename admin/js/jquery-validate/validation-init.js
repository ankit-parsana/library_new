var Script = function () {
    $().ready(function() {
        // validate the comment form when it is submitted
        $("#commentForm").validate();

        // validate pdf book form
        $("#pdf_form").validate({
            rules: {
                name: "required",
                type: "required",
                ages: "required",
                std: "required",
                sub: "required",
                pdf: "required",
            },
            messages: {
                name: "Please enter book name",
                type: "Please select book type",
                ages: "Please select book ages",
                std: "Please select book standard",
                sub: "Please select book subject",
                pdf: "Please select pdf file",
            }
        });
        
        // validate photo book form
        $("#photo_form").validate({
            rules: {
                name: "required",
                type: "required",
                ages: "required",
                std: "required",
                sub: "required",
                total: "required",
            },
            messages: {
                name: "Please enter book name",
                type: "Please select book type",
                ages: "Please select book ages",
                std: "Please select book standard",
                sub: "Please select book subject",
                total: "Please enter Total No. of page",
            }
        });
        
        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                firstname: "required",
                lastname: "required",
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                topic: {
                    required: "#newsletter:checked",
                    minlength: 2
                },
                agree: "required"
            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                agree: "Please accept our policy"
            }
        });

        // propose username by combining first- and lastname
        $("#username").focus(function() {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if(firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });

        //code to hide topic selection, disable for demo
        var newsletter = $("#newsletter");
        // newsletter topics are optional, hide at first
        var inital = newsletter.is(":checked");
        var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
        var topicInputs = topics.find("input").attr("disabled", !inital);
        // show when newsletter is checked
        newsletter.click(function() {
            topics[this.checked ? "removeClass" : "addClass"]("gray");
            topicInputs.attr("disabled", !this.checked);
        });
    });


}();
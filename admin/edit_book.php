<?php
include('session.php');
$id = $_GET['id'];
$type = $_GET['t'];
$query_type = "SELECT * FROM type ORDER BY type ASC";
$result_type = mysqli_query($con, $query_type);

$query_std = "SELECT * FROM standard";
$result_std = mysqli_query($con, $query_std);

$query_sub = "SELECT * FROM subject ORDER BY subject ASC";
$result_sub = mysqli_query($con, $query_sub);

$query_age = "SELECT * FROM age";
$result_age = mysqli_query($con, $query_age);

$query_edit = "SELECT * FROM book WHERE id='" . $id . "'";
$result_edit = mysqli_query($con, $query_edit);
$row_edit = $result_edit->fetch_assoc();

if (isset($_POST['submit'])) {
    if (count($_POST["type"])) {
        $type = implode(",", $_POST["type"]);
        //$type = $_POST['type'];
    }
    if (count($_POST["age"])) {
        $age = implode(",", $_POST["age"]);
        //$age = $_POST['age'];
    }
    if (count($_POST["std"])) {
        $std = implode(",", $_POST["std"]);
        //$std = $_POST['std'];
    }
    if (count($_POST["sub"])) {
        $sub = implode(",", $_POST["sub"]);
        //$sub = $_POST['sub'];
    }

    $type = $type . ",";
    $age = $age . ",";
    $std = $std . ",";
    $sub = $sub . ",";

    $query_update = "UPDATE book SET type='" . $type . "', age='" . $age . "', standard='" . $std . "', subject='" . $sub . "' WHERE id='" . $id . "'";
    mysqli_query($con, $query_update);
    echo "<script type='text/javascript'>alert('Book detail updated.')
	window.location.href='all_book.php';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Edit Book Detail</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Edit Book Detail
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal" id="pdf_form" method="post" action="">
                                            <div class="form-group ">
                                                <label for="name" class="control-label col-lg-3">Book Name</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="firstname" name="name" type="text" value="<?php echo $row_edit['name']; ?>" disabled />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="type" class="control-label col-lg-3">Select Type</label>
                                                <div class="col-lg-6">
                                                    <select name="type[]" class="form-control" multiple="multiple">
                                                        <option value="<?php echo $row_edit['type']; ?>"><?php echo $row_edit['type']; ?></option>
                                                        <?php 
                                                        $str_type = $row_edit['type'];
                                                        while ($row_type = $result_type->fetch_assoc()) {
                                                            $type_id = '/0'.$row_type['id'].',/';
                                                        ?>
                                                            <option value="0<?php echo $row_type['id']; if(preg_match($type_id,$str_type)) { echo '" selected="true'; }?>" ><?php echo $row_type['type']; ?></option>
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="age" class="control-label col-lg-3">Select Ages</label>
                                                <div class="col-lg-6">
                                                    <select name="age[]" class="form-control" multiple="multiple">
                                                        <option value="<?php echo $row_edit['age']; ?>"><?php echo $row_edit['age']; ?></option>
                                                        <?php 
                                                            $str_age = $row_edit['age'];
                                                            while ($row_age = $result_age->fetch_assoc()) { 
                                                            $age_id = '/0'.$row_age['age'].',/';
                                                                ?>
                                                                <option value="0<?php echo $row_age['age']; if(preg_match($age_id,$str_age)) { echo '" selected="true'; }?>"><?php echo $row_age['age']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="std" class="control-label col-lg-3">Select Standard</label>
                                                <div class="col-lg-6">
                                                    <select name="std[]" class="form-control" multiple="multiple">
                                                        <option value="<?php echo $row_edit['standard']; ?>"><?php echo $row_edit['standard']; ?></option>
                                                        <?php 
                                                            $str_std = $row_edit['standard'];
                                                            while ($row_std = $result_std->fetch_assoc()) { 
                                                            $std_id = '/0'.$row_std['standard'].',/';
                                                            ?>
                                                            <option value="0<?php echo $row_std['standard']; if(preg_match($std_id,$str_std)) { echo '" selected="true'; } ?>"><?php echo $row_std['standard']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="sub" class="control-label col-lg-3">Select Subject</label>
                                                <div class="col-lg-6">
                                                    <select name="sub[]" class="form-control" multiple="multiple">
                                                        <option value="<?php echo $row_edit['subject']; ?>"><?php echo $row_edit['subject']; ?></option>
                                                        <?php 
                                                            $str_sub = $row_edit['subject'];
                                                            while ($row_sub = $result_sub->fetch_assoc()) { 
                                                            $sub_id = '/0'.$row_sub['id'].',/';    
                                                            ?>
                                                            <option value="0<?php echo $row_sub['id']; if(preg_match($sub_id,$str_sub)) { echo '" selected="true'; } ?>"><?php echo $row_sub['subject']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <?php include('bottom.php'); ?>

    </body>
</html>
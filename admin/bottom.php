<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="js/lib/jquery.js"></script>
<script src="assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>

<script type="text/javascript" src="js/jquery-validate/jquery.validate.min.js"></script>
<script src="js/jquery-validate/validation-init.js"></script>

<script src="js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>

<!--clock init-->
<script src="assets/css3clock/js/script.js"></script>
<script src="assets/bootstrap-switch-master/build/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="assets/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="assets/jquery-tags-input/jquery.tagsinput.js"></script>
<!--<script src="js/custom-select/jquery.customSelect.min.js" ></script>-->
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>

<script src="js/scripts.js"></script>
<script>
    $(document).ready(function() {
    $("#category1").change(function() {
        var category1 = $("#category1").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category1=" + category1,
            success: function(data) {
                $("#input1_1").html(data);
                $("#input1_2").html(data);
                $("#input1_3").html(data);
            }
        });
    });
    $("#category2").change(function() {
        var category2 = $("#category2").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category2=" + category2,
            success: function(data) {
                $("#input2_1").html(data);
                $("#input2_2").html(data);
                $("#input2_3").html(data);
            }
        });
    });
    $("#category3").change(function() {
        var category3 = $("#category3").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category3=" + category3,
            success: function(data) {
                $("#input3_1").html(data);
                $("#input3_2").html(data);
                $("#input3_3").html(data);
            }
        });
    });
    $("#category4").change(function() {
        var category4 = $("#category4").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category4=" + category4,
            success: function(data) {
                $("#input4_1").html(data);
                $("#input4_2").html(data);
                $("#input4_3").html(data);
            }
        });
    });
    $("#category5").change(function() {
        var category5 = $("#category5").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category5=" + category5,
            success: function(data) {
                $("#input5_1").html(data);
                $("#input5_2").html(data);
                $("#input5_3").html(data);
            }
        });
    });
    $("#category6").change(function() {
        var category6 = $("#category6").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category6=" + category6,
            success: function(data) {
                $("#input6_1").html(data);
                $("#input6_2").html(data);
                $("#input6_3").html(data);
            }
        });
    });
    $("#category7").change(function() {
        var category7 = $("#category7").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category7=" + category7,
            success: function(data) {
                $("#input7_1").html(data);
                $("#input7_2").html(data);
                $("#input7_3").html(data);
            }
        });
    });
    $("#category8").change(function() {
        var category8 = $("#category8").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category8=" + category8,
            success: function(data) {
                $("#input8_1").html(data);
                $("#input8_2").html(data);
                $("#input8_3").html(data);
            }
        });
    });
    $("#category9").change(function() {
        var category9 = $("#category9").val();
        $.ajax({
            type: "post",
            url: "getbook.php",
            data: "category9=" + category9,
            success: function(data) {
                $("#input9_1").html(data);
                $("#input9_2").html(data);
                $("#input9_3").html(data);
            }
        });
    });
});
</script>
<script src="js/editable-table/table-editable.js"></script>

<script src="js/toggle-button/toggle-init.js"></script>
<script src="js/advanced-form/advanced-form.js"></script>
<!--script for this page-->

<script>
    jQuery(document).ready(function() {
        EditableTable.init();
    });    
</script>
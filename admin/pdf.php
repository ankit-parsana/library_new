<?php
include('session.php');

$query_type = "SELECT * FROM type ORDER BY type ASC";
$result_type = mysqli_query($con, $query_type);

$query_std = "SELECT * FROM standard";
$result_std = mysqli_query($con, $query_std);

$query_sub = "SELECT * FROM subject ORDER BY subject ASC";
$result_sub = mysqli_query($con, $query_sub);

$query_age = "SELECT * FROM age";
$result_age = mysqli_query($con, $query_age);

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    if (count($_POST["type"])) {
        $type = implode(",", $_POST["type"]);
        //$type = $_POST['type'];
    }
    if (count($_POST["age"])) {
        $age = implode(",", $_POST["age"]);
        //$age = $_POST['age'];
    }
    if (count($_POST["std"])) {
        $std = implode(",", $_POST["std"]);
        //$std = $_POST['std'];
    }
    if (count($_POST["sub"])) {
        $sub = implode(",", $_POST["sub"]);
        //$sub = $_POST['sub'];
    }
    //$total = $_POST['total'];
    
    $type = $type.",";
    $age = $age.",";
    $std = $std.",";
    $sub = $sub.",";

    $query_insert = "INSERT INTO book(name,type,age,standard,subject,book_type) VALUES('" . $name . "','" . $type . "','" . $age . "','" . $std . "','" . $sub . "','pdf')";
    mysqli_query($con, $query_insert);
    echo "<script type='text/javascript'>alert('Book detail add sucessfully.Upload PDF in next page.')
	window.location.href='uploadpdf.php?bname=".$name."';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="images/favicon.html">
        <title>:: E-Library :: Add New PDF Book</title>
        <?php include('head.php'); ?>

    </head>
    <body>
        <section id="container">
            <?php include('navbar.php'); ?>
            <?php include('sidebar.php'); ?>
            <!--main content start-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Add New PDF Book
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-cog" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div class="panel-body">
                                    <div class="form">
                                        <form class="cmxform form-horizontal" id="pdf_form" method="post" action="">
                                            <div class="form-group ">
                                                <label for="name" class="control-label col-lg-3">Book Name</label>
                                                <div class="col-lg-6">
                                                    <input class=" form-control" id="firstname" name="name" type="text" />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="type" class="control-label col-lg-3">Select Type</label>
                                                <div class="col-lg-6">
                                                    <select name="type[]" class="form-control" multiple="multiple">
                                                        <option value="">All Book Type</option>
                                                        <?php while ($row_type = $result_type->fetch_assoc()) { ?>
                                                            <option value="0<?php echo $row_type['id']; ?>"><?php echo $row_type['type']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="age" class="control-label col-lg-3">Select Ages</label>
                                                <div class="col-lg-6">
                                                    <select name="age[]" class="form-control" multiple="multiple">
                                                        <option value="">All Ages</option>
                                                        <?php while ($row_age = $result_age->fetch_assoc()) { ?>
                                                            <option value="0<?php echo $row_age['age']; ?>"><?php echo $row_age['age']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="std" class="control-label col-lg-3">Select Standard</label>
                                                <div class="col-lg-6">
                                                    <select name="std[]" class="form-control" multiple="multiple">
                                                        <option value="">All Standard</option>
                                                        <?php while ($row_std = $result_std->fetch_assoc()) { ?>
                                                            <option value="0<?php echo $row_std['standard']; ?>"><?php echo $row_std['standard']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="sub" class="control-label col-lg-3">Select Subject</label>
                                                <div class="col-lg-6">
                                                    <select name="sub[]" class="form-control" multiple="multiple">
                                                        <option value="">All Subject</option>
                                                        <?php while ($row_sub = $result_sub->fetch_assoc()) { ?>
                                                            <option value="0<?php echo $row_sub['id']; ?>"><?php echo $row_sub['subject']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
<!--                                            <div class="form-group ">
                                                <label for="total" class="control-label col-lg-3">Total No. of Chapter</label>
                                                <div class="controls col-lg-6">                                            
                                                    <input class=" form-control" id="total" name="total" type="text" />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label for="pdf" class="control-label col-lg-3">Upload PDF</label>
                                                <div class="controls col-lg-6">                                            
                                                    <input type="file" class="default" name="pdf" />
                                                </div>
                                            </div>-->
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-6">
                                                    <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </section>
            <!--main content end-->
        </section>

        <?php include('bottom.php'); ?>

    </body>
</html>
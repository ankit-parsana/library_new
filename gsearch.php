<!--
You are free to copy and use this sample in accordance with the terms of the
Apache license (http://www.apache.org/licenses/LICENSE-2.0.html)
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Google e Book Search | Hi-Lab Solution</title>
    <script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
    <script type="text/javascript">
    /*
    *  How to do a search that returns the max number of results per page.
    */
    
    google.load('search', '1');
    
    function OnLoad() {
    
      // create a search control
      var searchControl = new google.search.SearchControl();
    
      // Set the Search Control to get the most number of results
      searchControl.setResultSetSize(google.search.Search.LARGE_RESULTSET);
    
      // Create 2 searchers and add them to the control
      //searchControl.addSearcher(new google.search.WebSearch());
      searchControl.addSearcher(new google.search.BookSearch());
    
      // Set the options to draw the control in tabbed mode
      var drawOptions = new google.search.DrawOptions();
      drawOptions.setDrawMode(google.search.SearchControl.DRAW_MODE_TABBED);
    
      // Draw the control onto the page
      searchControl.draw(document.getElementById("content"), drawOptions);
    
      // Search!
      searchControl.execute("search book on google");
    }
    google.setOnLoadCallback(OnLoad);
    </script>
  </head>
  <body style="font-family: Arial;border: 0 none;">
    <div align="center" id="content">Loading...</div>
  </body>
</html>
<?php
include('admin/config.php');
$book_id = $_GET['bid'];

$query_book = "SELECT * FROM book WHERE id='$book_id'";
$result_book = mysqli_query($con, $query_book);
$row_book = $result_book->fetch_assoc();
//echo "<pre>".print_r($row_book);

$query_page = "SELECT image_name FROM user_uploads WHERE user_id='" . $book_id . "' ORDER BY id ASC";
$rs_page = mysqli_query($con, $query_page);
while ($row_page = $rs_page->fetch_assoc()) {
    $page_no[] = $row_page['image_name'];
    //echo "<pre>".print_r($row_page);
}
$page_cnt = $rs_page->num_rows;
?>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e - library</title>

        <link rel="stylesheet" href="css/style.css" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>

        <!--[if IE]>
        <script src="js/html5.js" type="text/javascript"></script>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
        <![endif]-->

        <!--[if IE]><script src="js/selectivizr.js"></script><![endif]-->
        <script src="js/jquery.js"></script>
        <script>
            $(document).bind("mobileinit", function() {
                $.mobile.ajaxEnabled = false;
            });
        </script>
        <script src="js/jquery.mobile-1.2.0.min.js"></script>
        <script src="js/turn.min.js"></script>
        <script src="js/onload.js"></script>

    </head>
    <body>

        <!-- BEGIN PAGE -->
        <div id="page">

            <!-- BEGIN ABOUT -->
            <article id="about">

                <ul>
                    <li>
                        <h3>How to read a book?</h3>
                        <p>Nulla congue pulvinar pharetra. Cras sed malesuada arcu. Duis eleifend nunc laoreet odio dapibus ac convallis sapien ornare. Nullam a est id diam elementum rhoncus.</p>
                    </li>

                    <li>
                        <h3>Short description</h3>
                        <p>Nulla congue pulvinar pharetra. Cras sed malesuada arcu. Duis eleifend nunc laoreet odio dapibus ac convallis sapien ornare. Nullam a est id diam elementum rhoncus.</p>
                    </li>

                    <li>
                        <h3>Start reading!</h3>
                        <p>Nulla congue pulvinar pharetra. Cras sed malesuada arcu. Duis eleifend nunc laoreet odio dapibus ac convallis sapien ornare. Nullam a est id diam elementum rhoncus.</p>
                    </li>
                </ul>

            </article>
            <!-- END ABOUT -->

            <!-- BEGIN BOOK -->
            <div id="book">
                <div id="cover">
                    <img src="upload/<?php echo $row_book['name']; ?>/<?php echo $page_no[0]; ?>" alt="Book Cover" id="logo-cover" />
                </div>
                <?php 
                    $i=0;
                    foreach ($page_no as $pageno) { 
                ?>
                    <div style="background-image:url(upload/<?php echo $row_book['name']; ?>/<?php echo $pageno; ?>);">
                        <div class="meta left">
                            <span class="num"><?php echo $i; ?></span>
                            <span class="description"><?php echo $row_book['name']; ?></span>
                        </div>
                    </div>
                <?php $i++; } ?>
                <div id="end">
                    <p>The End</p>
                </div>
            </div><!-- END BOOK -->

            <a class="nav_arrow prev"></a>
            <a class="nav_arrow next"></a>

        </div>
        <!-- END PAGE -->


        <!-- BEGIN FOOTER -->
        <footer id="footer">

            <a href="index.php" id="logo">
                <img src="img/home.png" alt="" style=" width: 45px; " />
            </a>

            <nav id="center" class="menu">
                <ul>
                    <li>
                        <a href="upload/<?php echo $row_book['name']; ?>/<?php echo $row_book['name']; ?>.zip" class="download" title="Download (ZIP)"></a>
                    </li>
                    <li>
                        <a class="zoom_in" title="Zoom In"></a>
                    </li>
                    <li>
                        <a class="zoom_out" title="Zoom Out"></a>
                    </li>
                    <li>
                        <a class="zoom_auto" title="Zoom Auto"></a>
                    </li>
                    <li>
                        <a class="zoom_original" title="Zoom Original (scale 1:1)"></a>
                    </li>
                    <li>
                        <a class="show_all" title="Show All upload/book1"></a>
                    </li>
                    <li>
                        <a class="home" title="Show Home Page"></a>
                    </li>

                </ul>
            </nav>

            <nav id="right" class="menu">
                <ul>
                    <li>
                        <a class="contact" title="Send Message"></a>
                    </li>
                    <li class="goto">
                        <label for="page-number">GO TO PAGE</label>
                        <input type="text" id="page-number" />
                        <button type="button">GO</button>
                    </li>
                </ul>
            </nav>

        </footer>
        <!-- END FOOTER -->



        <div class="overlay" id="contact">

            <form>
                <a class="close">X</a>

                <fieldset>
                    <h3>contact</h3>

                    <p>
                        <input type="text" value="name..." id="name" class="req" />
                    </p>

                    <p>
                        <input type="text" value="email..." id="email" class="req" />
                    </p>

                    <p>
                        <textarea id="message" class="req">message</textarea>
                    </p>

                    <p>
                        <button type="submit">Send Message</button>
                    </p>
                </fieldset>

                <fieldset class="thanks">
                    <h3>Message Sent</h3>
                    <p>This window will close in 5 seconds.</p>
                </fieldset>
            </form>

        </div>



        <!-- BEGIN ALL PAGES -->
        <div class="overlay" id="all_pages">

            <section class="container">

                <div id="menu_holder">

                    <ul id="slider">
                        <li class="page1">
                            <img src="upload/book1/1.jpg" alt="" />
                        </li>

                        <li class="page2">
                            <img src="upload/book1/2.jpg" alt="" />
                        </li>

                        <li class="page3">
                            <img src="upload/book1/3.jpg" alt="" />
                        </li>

                        <li class="page4">
                            <img src="upload/book1/4.jpg" alt="" />
                        </li>

                        <li class="page6">
                            <img src="upload/book1/5.jpg" alt="" />
                        </li>

                        <li class="page8">
                            <img src="upload/book1/6.jpg" alt="" />
                        </li>

                        <li class="page9">
                            <img src="upload/book1/7.jpg" alt="" />
                        </li>

                        <li class="page10">
                            <img src="upload/book1/8.jpg" alt="" />
                        </li>

                        <li class="page12">
                            <img src="upload/book1/9.jpg" alt="" />
                        </li>

                        <li class="page14">
                            <img src="upload/book1/10.jpg" alt="" />
                        </li>

                        <li class="page14">
                            <img src="upload/book1/11.jpg" alt="" />
                        </li>

                        <li class="page14">
                            <img src="upload/book1/12.jpg" alt="" />
                        </li>

                        <li class="page16">
                            <img src="pages/thumbs/end.png" alt="" />
                        </li>
                    </ul>
                </div>

            </section>

        </div>
        <!-- END ALL PAGES -->


    </body>
</html>
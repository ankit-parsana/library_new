/*-------------script for home page-------------*/
$(document).ready(function() {
    /*$('#mydate').datePicker();*/

    /*$('.dock').css('bottom', '-20%');*/

    $('#dock2').Fisheye(
            {
                maxWidth: 50,
                items: 'a',
                itemsText: 'span',
                container: '.dock-container2',
                itemWidth: 100,
                proximity: 60,
                alignment: 'left',
                valign: 'bottom',
                halign: 'center'
            }
    )

    $('#dock1').Fisheye(
            {
                maxWidth: 50,
                items: 'a',
                itemsText: 'span',
                container: '.dock-container1',
                itemWidth: 100,
                proximity: 60,
                alignment: 'left',
                valign: 'bottom',
                halign: 'center'
            }
    )


    if ((screen.width) == 1600)
    {
        $('#dock1').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container1',
                    itemWidth: 90,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
        $('#dock2').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container2',
                    itemWidth: 90,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
    }
    else if ((screen.width) == 1366)
    {
        $('#dock1').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container1',
                    itemWidth: 75,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
        $('#dock2').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container2',
                    itemWidth: 75,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
    }
    else if ((screen.width) == 1280 && (screen.height) == 1024)
    {
        /*$('.rack').css('top', '45%');*/

        $('.shelf3_container').css('margin', '7% 0 0 2%');
        $('.shelf3_block1_container,.shelf3_block2_container,.shelf3_block3_container').css('margin', '3% 0 0 0');
        $('.shelf3_block4_container,.shelf3_block5_container,.shelf3_block6_container').css('margin', '5% 0 0 0');
        $('.shelf3_block7_container,.shelf3_block8_container,.shelf3_block9_container').css('margin', '6% 0 0 0');

        $('#dock1').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container1',
                    itemWidth: 70,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
        $('#dock2').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container2',
                    itemWidth: 70,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
    }
    else if ((screen.width) == 1280 && (screen.height) == 800)
    {
        $('#dock1').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container1',
                    itemWidth: 70,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
        $('#dock2').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container2',
                    itemWidth: 70,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
    }
    else if ((screen.width) == 1152)
    {
        $('#btnSearch').css('background-size', '100% 90%');
        $('#btnFilter').css('background-size', '100% 90%');
        $('#btnSearchSmall').css('background-size', '100% 90%');
        $('#btnSort').css('background-size', '100% 90%');

        $('#dock1').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container1',
                    itemWidth: 60,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
        $('#dock2').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container2',
                    itemWidth: 60,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
    }
    else if ((screen.width) == 1024)
    {
        $('#dock1').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container1',
                    itemWidth: 50,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
        $('#dock2').Fisheye(
                {
                    maxWidth: 50,
                    items: 'a',
                    itemsText: 'span',
                    container: '.dock-container2',
                    itemWidth: 50,
                    proximity: 40,
                    alignment: 'left',
                    valign: 'bottom',
                    halign: 'center'
                }
        )
    }


    $('#left_dock_arrow').click(function() {

        /*$('.dock-container2').animate({height:'100%'});*/
        /*$('.dock').fadeIn('fast');*/
        $('#dock2').removeAttr('style');
        /*$('.dock').animate({bottom:'0'});*/
        $('#dock2').fadeIn('fast');


    });

    $('#dock2').hover(function() {

    }, function() {
        /*$('.dock-container2').animate({height:'0%'});*/
        $('#dock2').fadeOut('slow');
        /*$('.dock').animate({bottom:'-20%'});*/
        $('#dock2').attr('style', 'visibility:hidden');
    });

    $('#right_dock_arrow').click(function() {

        /*$('.dock-container2').animate({height:'100%'});*/
        /*$('.dock').fadeIn('fast');*/
        $('#dock1').removeAttr('style');
        /*$('.dock').animate({bottom:'0'});*/
        $('#dock1').fadeIn('fast');


    });

    $('#dock1').hover(function() {

    }, function() {
        /*$('.dock-container2').animate({height:'0%'});*/
        $('#dock1').fadeOut('slow');
        /*$('.dock').animate({bottom:'-20%'});*/
        $('#dock1').attr('style', 'visibility:hidden');
    });


    $('#book_shelf').hide();

    $('#loader').show();


    $(window).bind('load', function() {

        $('#loader').hide();

        $('#book_shelf').fadeIn('slow', function() {
            /*setTimeout(bookRackShow);*/
            setTimeout(showShelf1, 500);
            setTimeout(showShelf2, 1000);
            setTimeout(showShelf3, 1700);

            $("#datepicker").datepicker({
            }).datepicker().fadeIn('slow');
        });

    });

    /**********Book Bubble***********/

    /*alert($('#bbook1').attr('class'));*/


    /*jQuery('#book_shelf').each(function(i)
     {
     $('#book' + i).hover(function() {
     alert(i);
     $('#bbook' + i).css('display', 'block');
     $('#bbook' + i).animate({width:'150%'});
     $('#bbt' + i).animate({fontSize:'20px'});
     
     }, function () {
     $('#bbook' + i).stop();
     $('#bbook' + i).animate({width:'1%'});
     $('#bbt' + i).animate({fontSize:'0px'}, function () {
     $('#bbook' + i).css('display', 'none');	
     });
     });
     });*/



    for (var i = 1; i <= 10000; i++)
    {
        bubble(i);

    }

});

var i;
function bubble(i)
{
    $('#book' + i).hover(function() {

        $('#bbook' + i).css('display', 'block');
        if ((screen.width) == 1152)
        {


            $('#bbook' + i).animate({width: '120%'}, 1000);
            $('#bbt' + i).animate({fontSize: '14px'}, 1000);
        }
        else
        {


            $('#bbook' + i).animate({width: '150%'}, 1000);
            $('#bbt' + i).animate({fontSize: '20px'}, 1000);
        }


    }, function() {
        $('#bbook' + i).stop();



        $('#bbook' + i).animate({width: '1%'});
        $('#bbt' + i).animate({fontSize: '0px'}, function() {
            $('#bbook' + i).css('display', 'none');
        });
    });
}
/*function bookRackShow()
 {
 //$('.rack').css('visibility', 'visible');
 $('.rack').removeAttr('style');
 
 if((screen.width) == 1280 && (screen.height) == 1024)
 {
 $('.rack').css('display', 'none');
 $('.rack').css('width', '25%');
 $('.rack').css('left', '-7.5%');
 $('.rack').fadeIn('slow');	
 }
 else
 {
 
 $('.rack').css('display', 'none');
 $('.rack').fadeIn('slow');
 }
 }*/

function showShelf1()
{
    $('#shelf1').fadeIn('slow');
}

function showShelf2()
{
    $('#shelf2').fadeIn('slow');
}

function showShelf3()
{
    $('#shelf3').fadeIn('slow');
}

/******functions for buttons***************/
function searchDown()
{
    $('#btnSearch').css('background', 'url(assest/images/my-20corner-20slices/btn search clicked.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnSearch').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnSearch').css('background-size', '100% 100%');
    }
}
function searchUp()
{
    $('#btnSearch').css('background', 'url(assest/images/my-20corner-20slices/btn search.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnSearch').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnSearch').css('background-size', '100% 100%');
    }
}

function filterDown()
{
    $('#btnFilter').css('background', 'url(assest/images/my-20corner-20slices/btn search clicked.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnFilter').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnFilter').css('background-size', '100% 100%');
    }
}
function filterUp()
{
    $('#btnFilter').css('background', 'url(assest/images/my-20corner-20slices/btn search.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnFilter').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnFilter').css('background-size', '100% 100%');
    }
}

function searchSmallDown()
{
    $('#btnSearchSmall').css('background', 'url(assest/images/my-20corner-20slices/btn search clicked.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnSearchSmall').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnSearchSmall').css('background-size', '100% 100%');
    }
}
function searchSmallUp()
{
    $('#btnSearchSmall').css('background', 'url(assest/images/my-20corner-20slices/btn search small.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnSearchSmall').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnSearchSmall').css('background-size', '100% 100%');
    }
}

function sortDown()
{
    $('#btnSort').css('background', 'url(assest/images/my-20corner-20slices/sort_clicked.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnSort').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnSort').css('background-size', '100% 100%');
    }
}
function sortUp()
{
    $('#btnSort').css('background', 'url(assest/images/my-20corner-20slices/sort.png) no-repeat');
    if ((screen.width) == 1152)
    {
        $('#btnSort').css('background-size', '100% 90%');
    }
    else
    {
        $('#btnSort').css('background-size', '100% 100%');
    }
}


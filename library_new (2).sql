-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2014 at 03:22 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `library_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `age`
--

CREATE TABLE IF NOT EXISTS `age` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `age` varchar(250) NOT NULL,
  `search_display` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `age`
--

INSERT INTO `age` (`id`, `age`, `search_display`) VALUES
(1, '3', 'y'),
(2, '4', 'y'),
(3, '5', 'y'),
(4, '6', 'y'),
(5, '7', 'y'),
(6, '8', 'y'),
(7, '9', 'y'),
(8, '10', 'y'),
(9, '11', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `asign_book`
--

CREATE TABLE IF NOT EXISTS `asign_book` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `cat1` varchar(255) NOT NULL,
  `row_1_1` varchar(255) NOT NULL,
  `row_1_2` varchar(255) NOT NULL,
  `row_1_3` varchar(255) NOT NULL,
  `row_1_4` varchar(255) NOT NULL,
  `row_1_5` varchar(255) NOT NULL,
  `row_1_6` varchar(255) NOT NULL,
  `row_1_7` varchar(255) NOT NULL,
  `row_1_8` varchar(255) NOT NULL,
  `row_1_9` varchar(255) NOT NULL,
  `row_2_1` varchar(255) NOT NULL,
  `row_2_2` varchar(255) NOT NULL,
  `row_2_3` varchar(255) NOT NULL,
  `row_2_4` varchar(255) NOT NULL,
  `row_2_5` varchar(255) NOT NULL,
  `row_2_6` varchar(255) NOT NULL,
  `row_2_7` varchar(255) NOT NULL,
  `row_2_8` varchar(255) NOT NULL,
  `row_2_9` varchar(255) NOT NULL,
  `row_3_1` varchar(255) NOT NULL,
  `row_3_2` varchar(255) NOT NULL,
  `row_3_3` varchar(255) NOT NULL,
  `row_3_4` varchar(255) NOT NULL,
  `row_3_5` varchar(255) NOT NULL,
  `row_3_6` varchar(255) NOT NULL,
  `row_3_7` varchar(255) NOT NULL,
  `row_3_8` varchar(255) NOT NULL,
  `row_3_9` varchar(255) NOT NULL,
  `cat2` varchar(255) NOT NULL,
  `cat3` varchar(255) NOT NULL,
  `cat4` varchar(255) NOT NULL,
  `cat5` varchar(255) NOT NULL,
  `cat6` varchar(255) NOT NULL,
  `cat7` varchar(255) NOT NULL,
  `cat8` varchar(255) NOT NULL,
  `cat9` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `asign_book`
--

INSERT INTO `asign_book` (`id`, `cat1`, `row_1_1`, `row_1_2`, `row_1_3`, `row_1_4`, `row_1_5`, `row_1_6`, `row_1_7`, `row_1_8`, `row_1_9`, `row_2_1`, `row_2_2`, `row_2_3`, `row_2_4`, `row_2_5`, `row_2_6`, `row_2_7`, `row_2_8`, `row_2_9`, `row_3_1`, `row_3_2`, `row_3_3`, `row_3_4`, `row_3_5`, `row_3_6`, `row_3_7`, `row_3_8`, `row_3_9`, `cat2`, `cat3`, `cat4`, `cat5`, `cat6`, `cat7`, `cat8`, `cat9`) VALUES
(1, 'kk', 'kk', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'kk', 'k', 'k');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `age` text NOT NULL,
  `standard` text NOT NULL,
  `subject` text NOT NULL,
  `page_no` int(10) NOT NULL,
  `book_type` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `name`, `type`, `age`, `standard`, `subject`, `page_no`, `book_type`) VALUES
(50, 'book1', '01,02,', '04,05,', '01,02,', '04,03,', 12, 'photo'),
(51, 'book2', '01,02,03,', '03,04,05,', '01,03,', '04,03,', 12, 'photo'),
(52, 'book3', '01,012,', '03,04,', '03,', '03,', 12, 'photo'),
(53, 'book4', '012,', '03,04,', '03,', '03,', 12, 'photo'),
(54, 'book5', '04,013,', '04,05,', '01,010,011,012,', '04,', 12, 'photo'),
(55, 'pdf1', '01,02,', '03,04,', '01,02,', '04,03,', 2, 'pdf'),
(56, 'fiction', '01,02,04,', '03,05,', '01,03,', '03,05,', 40, 'photo');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `standard` int(10) NOT NULL,
  `main_search` int(3) NOT NULL,
  `search` int(3) NOT NULL,
  `bottom` int(3) NOT NULL,
  `school_name` text NOT NULL,
  `logo` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `username`, `password`, `standard`, `main_search`, `search`, `bottom`, `school_name`, `logo`) VALUES
(1, 'admin', 'admin', 0, 1, 1, 0, 'Little Flower School', 'school.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `standard`
--

CREATE TABLE IF NOT EXISTS `standard` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `standard` varchar(250) NOT NULL,
  `search_display` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `standard`
--

INSERT INTO `standard` (`id`, `standard`, `search_display`) VALUES
(1, '1', 'y'),
(2, '2', 'y'),
(3, '3', 'y'),
(4, '4', 'y'),
(5, '5', 'y'),
(6, '6', 'y'),
(7, '7', 'y'),
(8, '8', 'y'),
(9, '9', 'y'),
(10, '10', 'y'),
(11, '11', 'y'),
(12, '12', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) NOT NULL,
  `search_display` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `subject`, `search_display`) VALUES
(1, 'Maths', 'y'),
(2, 'Science', 'y'),
(3, 'GK', 'y'),
(4, 'English', 'y'),
(5, 'History', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `search_display` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`, `search_display`) VALUES
(1, 'Able Infants', 'y'),
(2, 'Audio', 'y'),
(3, 'Biography', 'y'),
(4, 'Fiction', 'y'),
(12, 'Myths and Legends', 'y'),
(13, 'Non-fiction', 'y');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
$query_type = "SELECT * FROM type WHERE search_display='y' ORDER BY type ASC";
$result_type = mysqli_query($con, $query_type);

$query_std = "SELECT * FROM standard WHERE search_display='y'";
$result_std = mysqli_query($con, $query_std);

$query_sub = "SELECT * FROM subject WHERE search_display='y' ORDER BY subject ASC";
$result_sub = mysqli_query($con, $query_sub);

$query_age = "SELECT * FROM age WHERE search_display='y'";
$result_age = mysqli_query($con, $query_age);

$query_setting = "SELECT main_search,search FROM setting WHERE id=1";
$result_setting = mysqli_query($con, $query_setting);
$row_setting = $result_setting->fetch_assoc();

$main_search_enable = $row_setting['main_search'];
$search_enable = $row_setting['search'];?>
<a href="index.php" class="home_btn"><img src="img/home.png" width="32" height="32"></a>
<?php if ($main_search_enable == 1) {  
    ?>
    <div class="search_shelf_books_container">
        <input type="text" id="search" autocomplete="off" class="search_textbox" name="txtSearch" placeholder="Book Search" />
        <input type="button" class="search_button" id="btnSearch" name="btnSearch" onmousedown="searchDown()" onmouseup="searchUp()" />
        <!-- Show Results -->
        <h4 id="results-text">Showing results for: <b id="search-string">Array</b></h4>
        <ul id="results"></ul>
    </div>
    <?php
}
if ($search_enable == 1) {
    ?>
    <div class="search-wrapper">
        <form name="search_form" method="post" action="search.php?chk=sub_search">
            <div class="s-left f-left">
                <div class="">
                    <select class="s-select" name="age" id="age">
                        <option value="">All Ages</option>
                        <?php while ($row_age = $result_age->fetch_assoc()) { ?>
                            <option value="0<?php echo $row_age['age']; ?>"><?php echo $row_age['age']; ?></option>
                        <?php } ?>
                    </select>
                    <select class="s-select" name="type" id="type">
                        <option value="">All Book Type</option>
                        <?php while ($row_type = $result_type->fetch_assoc()) { ?>
                            <option value="0<?php echo $row_type['id']; ?>"><?php echo $row_type['type']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="">
                    <select class="s-select" name="std" id="std">
                        <option value="">All Standard</option>
                        <?php while ($row_std = $result_std->fetch_assoc()) { ?>
                            <option value="0<?php echo $row_std['standard']; ?>"><?php echo $row_std['standard']; ?></option>
                        <?php } ?>
                    </select>
                    <select class="s-select" name="sub" id="sub">
                        <option value="">All Subject</option>
                        <?php while ($row_sub = $result_sub->fetch_assoc()) { ?>
                            <option value="0<?php echo $row_sub['id']; ?>"><?php echo $row_sub['subject']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="s-right">
                <input class="s-btn" type="submit" name="search" value=""></input>
            </div>
        </form>
    </div>
<?php
}
if ($search_enable == 0) {
    ?>
    <style>
        .shelf_books_container {
            top: 14% !important;
        }
    </style>
<?php
}
if ($main_search_enable == 0 && $search_enable == 0) {
    ?>
    <style>
        .shelf_books_container {
            top: 2% !important;
        }
    </style>
<?php
}
if ($main_search_enable == 0) {
    ?>
    <style>
        .shelf_books_container {
            top: 19% !important;
        }
        .search-wrapper {
            margin-top: 22px !important;
        }
    </style>
<?php } ?>